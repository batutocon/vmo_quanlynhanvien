﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Common.Exceptions
{
    public class QuanLyNhanSu : Exception
    {
        public QuanLyNhanSu()
        {
        }

        public QuanLyNhanSu(string message)
            : base(message)
        {
        }

        public QuanLyNhanSu(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
