﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Common.Constants
{
    public class SystemConstants
    {
        public const string MainConnectionString = "QuanLyNhanSuSolutionDb";
        public const string CartSession = "CartSession";

        public class AppSettings
        {
            public const string DefaultLanguageId = "DefaultLanguageId";
            public const string Token = "Token";
            public const string BaseAddress = "BaseAddress";
        }

        public class Web
        {
            public const int TRANGTHAI_BANGLUONG_CHUACO = 1;
            public const int TRANGTHAI_BANGLUONG_TAOMOI = 2;
            public const int TRANGTHAI_BANGLUONG_TAMTINHLUONG = 3;
            public const int TRANGTHAI_BANGLUONG_THANHTOAN = 4;

            public const int TRANGTHAI_NHANVIEN_DANGLAMVIEC = 1;
            public const int TRANGTHAI_NHANVIEN_NGHIVIEC = 0;

            public const int TRANGTHAI_NHANVIEN_TSCP_DANGCHOMUON = 1;
            public const int TRANGTHAI_NHANVIEN_TSCP_DATRA = 0;

            public const int TRANGTHAI_HOPDONG_COHIEULUC = 1;
            public const int TRANGTHAI_HOPDONG_HUYHOPDONG = 2;
            public const int TRANGTHAI_HOPDONG_HETHIEULUC = 3;

            public const int TRANGTHAI_NGHIVIEC_CHOPHEDUYET = 1;
            public const int TRANGTHAI_NGHIVIEC_DAPHEDUYET = 2;

            public const int LOAILOG_NHANVIEN = 1;
            public const int LOAILOG_HOPDONG = 2;
        }

    }
}