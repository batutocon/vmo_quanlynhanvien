﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Common.Constants
{
    public class Utility
    {
        public static string ConvertToCurrencyVND(double number)
        {
            if (number == 0)
            {
                return "0";
            }
            return (number.ToString("#,###")) + " VNĐ";
        }
    }
}
