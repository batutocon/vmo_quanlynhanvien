﻿using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class KhenThuongViewModel
    {
        public int IdNhanVien { get; set; }
        public string NoiDungKhenThuong { get; set; }
        public double SoTien { get; set; }
        public int Thang { get; set; }
        public int Nam { get; set; }
        public string NguoiTao { get; set; }
    }
    public class KhenThuongViewDetailModel
    {
        public NhanVien nhanVien { get; set; }
        public List<KhenThuong> khenThuongs { get; set; }
    }
}
