﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class NhanVien_HopDongViewModel
    {
        public int Id { get; set; }
        public string TenNV { get; set; }
        public string TenLoaiHD { get; set; }
        public string Email { get; set; }
        [Required(ErrorMessage = "Mã hợp đồng không được để trống")]
        public string MaHopDong { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn loại hợp đồng")]
        public int IdLoaiHopDong { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn Nhân viên")]
        public int IdNhanVien { get; set; }
        [Required(ErrorMessage = "Ngày bắt đầu không được để trống")]
        public DateTime NgayBatDau { get; set; }
        [Required(ErrorMessage = "Ngày kết thúc không được để trống")]
        public DateTime NgayKetThuc { get; set; }
        [Required(ErrorMessage = "Ngày ký không được để trống")]
        public DateTime NgayKy { get; set; }
        [Required(ErrorMessage = "Người ký không được để trống")]
        public string NguoiKy { get; set; }
        [Required(ErrorMessage = "Chưa chọn trạng thái")]
        public int TrangThai { get; set; }
        [Required(ErrorMessage = "Người tạo không được để trống")]
        public double LuongCB { get; set; }
        [Required(ErrorMessage = "Chưa chọn hệ số lương")]
        public int IdHeSoLuong { get; set; }

    }
}