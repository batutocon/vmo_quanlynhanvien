﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_TinhThanhViewModel
    {
        [Required(ErrorMessage = "Mã quốc gia không được bỏ trống.")]
        public string MaQuocGia { set; get; }

        [Required(ErrorMessage = "Mã tỉnh thành không được bỏ trống.")]
        public string MaTinhThanh { set; get; }

        [Required(ErrorMessage = "Tên tỉnh thành không được bỏ trống.")]
        public string TenTinhThanh { set; get; }
        public int TrangThai { set; get; }
    }
}