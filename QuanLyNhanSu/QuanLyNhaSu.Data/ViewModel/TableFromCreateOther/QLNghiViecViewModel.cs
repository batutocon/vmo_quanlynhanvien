﻿using Microsoft.AspNetCore.Http;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class QLNghiViecViewModel
    {
        [Required(ErrorMessage = "Chưa chọn nhân viên")]
        public int IdNhanVien { get; set; }
        public string Note { get; set; }
        [Required(ErrorMessage = "Chưa chọn trạng thái")]
        public int TrangThai { get; set; }
        public string TaoBoi { get; set; }
    }

    public class QLNghiViecGetAllViewModel
    {
        public QLNghiViec qlnghiviec { get; set; }
        public NhanVien nhanVien { get; set; }
        public NhanVien_TSCP nhanVien_TSCP { get; set; }
        public List<DM_TaiSanCapPhat> dM_TaiSanCapPhats { get; set; }
    }
}