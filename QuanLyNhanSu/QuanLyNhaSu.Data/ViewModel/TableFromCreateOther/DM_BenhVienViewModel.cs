﻿using System.ComponentModel.DataAnnotations;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_BenhVienViewModel
    {
        [Required(ErrorMessage = "Mã bệnh viện không được bỏ trống.")]
        public string MaBenhVien { set; get; }

        [Required(ErrorMessage = "Tên bệnh viện không được bỏ trống.")]
        public string TenBenhVien { set; get; }

        [Required(ErrorMessage = "Địa chỉ không được bỏ trống.")]
        public string DiaChi { set; get; }
        public string MoTa { set; get; }

    }
}