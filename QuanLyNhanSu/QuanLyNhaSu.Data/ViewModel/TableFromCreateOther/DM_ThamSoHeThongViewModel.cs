﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_ThamSoHeThongViewModel
    {
        [Required(ErrorMessage = "Mã tham số không được bỏ trống.")]
        public string MaThamSo { set; get; }

        [Required(ErrorMessage = "Tên tham số không được bỏ trống.")]
        public string TenThamSo { set; get; }
        public string MoTa { set; get; }
        public int TrangThai { set; get; }

        [Required(ErrorMessage = "Giá trị tham số không được bỏ trống.")]
        public string GiaTri { set; get; }

        [Required(ErrorMessage = "Ngày tạo không được bỏ trống.")]
        public DateTime NgayTao { set; get; }

        [Required(ErrorMessage = "Ngày cập nhật không được bỏ trống.")]
        public DateTime NgayCapNhat { set; get; }

        [Required(ErrorMessage = "Người tạo không được bỏ trống.")]
        public string NguoiTao { set; get; }

        [Required(ErrorMessage = "Người cập nhật không được bỏ trống.")]
        public string NguoiCapNhat { set; get; }
    }
}