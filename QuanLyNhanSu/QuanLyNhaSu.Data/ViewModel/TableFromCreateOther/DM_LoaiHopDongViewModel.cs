﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_LoaiHopDongViewModel
    {
        [Required(ErrorMessage = "Mã loại hợp đồng không được bỏ trống.")]
        public string MaLoaiHopDong { set; get; }

        [Required(ErrorMessage = "Tên loại hợp đồng không được bỏ trống.")]
        public string TenLoaiHopDong { set; get; }

        [Required(ErrorMessage = "Thời hạn hợp đồng không được bỏ trống.")]
        public string ThoiHanHopDong { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
    }
}