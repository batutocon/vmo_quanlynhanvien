﻿using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class NhanVien_TSCPViewModel
    {
        public int IdNhanVien { get; set; }
        public string IdTSCP { get; set; }
        public int TrangThai { get; set; }
        public string NoiDung { get; set; }
        public string NguoiTao { get; set; }
    }
    public class NhanVien_TSCPUpdateViewModel
    {
        public int Id { get; set; }
        public string IdTSCP { get; set; }
        public int TrangThai { get; set; }
        public string NguoiCapNhat { get; set; }
        public string NoiDung { get; set; }
    }

    public class NhanVien_TSCP_DetailViewModel
    {
        public NhanVien nhanVien { get; set; }
        public NhanVien_TSCP nhanVien_TSCP { get; set; }
        public List<DM_TaiSanCapPhat> dM_TaiSanCapPhats { get; set; }
    }
}
