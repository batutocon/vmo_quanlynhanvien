﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_PhucLoiViewModel
    {
        [Required(ErrorMessage = "Mã chế độ phúc lợi không được bỏ trống.")]

        public string MaCheDoPhucLoi { set; get; }

        [Required(ErrorMessage = "Tên phúc lợi không được bỏ trống.")]
        public string TenPhucLoi { set; get; }
        [Required(ErrorMessage = "Số tiền trước thuế không được bỏ trống.")]

        public string SoTienTruocThue { set; get; }

        [Required(ErrorMessage = "Ngày hiệu lực không được bỏ trống.")]

        public DateTime NgayHieuLuc { set; get; }

        [Required(ErrorMessage = "Ngày hết hiệu lực không được bỏ trống.")]

        public DateTime NgayHetHieuLuc { set; get; }

        [Required(ErrorMessage = "Nguồn chi không được bỏ trống.")]

        public string NguonChi { set; get; }
        public int TrangThai { set; get; }
        public string MucChiToiDa { set; get; }

    }
}