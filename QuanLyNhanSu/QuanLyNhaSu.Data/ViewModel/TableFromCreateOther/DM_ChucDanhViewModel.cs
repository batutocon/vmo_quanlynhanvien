﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_ChucDanhViewModel
    {
        [Required(ErrorMessage = "Mã chức danh không được bỏ trống.")]
        public string MaChucDanh { set; get; }

        [Required(ErrorMessage = "Tên chức danh không được bỏ trống.")]
        public string TenChucDanh { set; get; }
        public string MoTa { set; get; }

        [Required(ErrorMessage = "Id đơn vị không được bỏ trống.")]
        public string IdDonVi { set; get; }
    }
}