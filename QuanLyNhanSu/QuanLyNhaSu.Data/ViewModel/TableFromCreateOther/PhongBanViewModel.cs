﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class PhongBanViewModel
    {
        public string MaPhongBan { get; set; }
        [Required(ErrorMessage = "Mã quốc gia không được bỏ trống.")]
        public string TenPhongBan { get; set; }
        public string MoTa { get; set; }
        [Required(ErrorMessage = "Mã quốc gia không được bỏ trống.")]
        public string ViTri { get; set; }

    }
}