﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_QuocGiaViewModel
    {
        [Required(ErrorMessage = "Mã quốc gia không được bỏ trống.")]
        public string MaQuocGia { set; get; }

        [Required(ErrorMessage = "Tên quốc gia không được bỏ trống.")]
        public string TenQuocGia { set; get; }
        public int TrangThai { set; get; }

    }
}