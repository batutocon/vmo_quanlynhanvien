﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_QuanHuyenViewModel
    {
        [Required(ErrorMessage = "Mã tỉnh thành không được bỏ trống.")]
        public string MaTinhThanh { set; get; }

        [Required(ErrorMessage = "Mã quận huyện không được bỏ trống.")]
        public string MaQuanHuyen { set; get; }

        [Required(ErrorMessage = "Tên quận huyện không được bỏ trống.")]
        public string TenQuanHuyen { set; get; }

        public int TrangThai { set; get; }
    }
}