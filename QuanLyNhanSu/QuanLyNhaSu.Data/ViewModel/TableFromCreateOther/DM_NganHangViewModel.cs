﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_NganHangViewModel
    {
        [Required(ErrorMessage = "Mã ngân hàng không được bỏ trống.")]
        public string MaNganHang { set; get; }

        [Required(ErrorMessage = "Tên ngân hàng không được bỏ trống.")]
        public string TenNganHang { set; get; }
        public string TenVietTat { set; get; }
        public int TrangThai { set; get; }

    }
}