﻿using System.ComponentModel.DataAnnotations;
namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_ChiNhanhNganHangViewModel
    {
        [Required(ErrorMessage = "Mã chi nhánh không được bỏ trống.")]
        public string MaChiNhanh { set; get; }

        [Required(ErrorMessage = "Tên chi nhánh không được bỏ trống.")]
        public string TenChiNhanh { set; get; }

        [Required(ErrorMessage = "Mã ngân hàng không được bỏ trống.")]
        public string MaNganHang { set; get; }

        [Required(ErrorMessage = "Tên ngân hàng không được bỏ trống.")]
        public string TenNganHang { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }

    }
}