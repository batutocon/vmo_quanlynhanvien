﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_TaiSanCapPhatViewModel
    {
        [Required(ErrorMessage = "Mã tài sản cấp phát không được bỏ trống.")]
        public string MaTaiSanCapPhat { set; get; }
        [Required(ErrorMessage = "Tên tài sản cấp phát không được bỏ trống.")]
        public string TenTaiSanCapPhat { get; set; }

        [Required(ErrorMessage = "Số lượng tài sản cấp phát không được bỏ trống.")]
        public int SoLuong { set; get; }
        public string MoTa { get; set; }
    }
}