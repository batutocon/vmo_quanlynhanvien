﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_XaPhuongViewModel
    {
        [Required(ErrorMessage = "Mã quận huyện không được bỏ trống.")]
        public string MaQuanHuyen { set; get; }

        [Required(ErrorMessage = "Mã xã phường không được bỏ trống.")]
        public string MaXaPhuong { set; get; }

        [Required(ErrorMessage = "Tên xã phường không được bỏ trống.")]
        public string TenXaPhuong { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
    }
}