﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DonViViewModel
    {
        public string MaDonVi { get; set; }
        [Required(ErrorMessage = "Tên đơn vị không được bỏ trống.")]
        public string TenDonVi { get; set; }
        public string MoTa { get; set; }
    }
}