﻿using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class KyLuatViewModel
    {
        public int IdNhanVien { get; set; }
        public string NoiDungKyLuat { get; set; }
        public double SoTien { get; set; }
        public int Thang { get; set; }
        public int Nam { get; set; }
        public string NguoiTao { get; set; }
    }
    public class KyLuatViewDetailModel
    {
        public NhanVien nhanVien { get; set; }
        public List<KyLuat> KyLuats { get; set; }
    }
}
