﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DM_PhuCapViewModel
    {
        [Required(ErrorMessage = "Mã phụ cấp không được bỏ trống.")]
        public string MaPhuCap { set; get; }

        [Required(ErrorMessage = "Loại thưởng không được bỏ trống.")]
        public string LoaiThuong { set; get; }

        [Required(ErrorMessage = "Người chi không được bỏ trống.")]
        public string NguonChi { set; get; }

        [Required(ErrorMessage = "Số tiền không được bỏ trống.")]
        public double SoTienTheoLuongCB { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
    }
}