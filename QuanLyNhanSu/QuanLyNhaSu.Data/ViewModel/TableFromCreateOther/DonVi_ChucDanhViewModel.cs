﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.ViewModel
{
    public class DonVi_ChucDanhViewModel
    {
        public DM_ChucDanh ChucDanh { set; get; }
        public DonVi DonVi { set; get; }
    }
}