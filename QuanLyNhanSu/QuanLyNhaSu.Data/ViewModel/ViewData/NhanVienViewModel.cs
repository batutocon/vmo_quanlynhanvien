﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.ViewModel.ViewData
{
    public class NhanVienViewModel
    {
        public NhanVien nhanvien { get; set; }
        public DM_ChucDanh chucdanh { get; set; }
        public PhongBan phongban { get; set; }

    }
}