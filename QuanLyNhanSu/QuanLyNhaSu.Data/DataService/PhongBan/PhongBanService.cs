﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class PhongBanService : IPhongBanService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public PhongBanService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(PhongBanViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.PhongBan()
            {
                MaPhongBan = request.MaPhongBan,
                TenPhongBan = request.TenPhongBan,
                MoTa = request.MoTa,
                ViTri = request.ViTri,
              
            };
            _context.PhongBans.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaPhongBan = "PB00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaPhongBan = "PB0" + obj.Id;
            }
            else
            {
                obj.MaPhongBan = "PB" + obj.Id;
            }
            _context.PhongBans.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(PhongBan request)
        {
            var obj = await _context.PhongBans.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Phòng ban không tồn tại {request.Id}");
            obj.MaPhongBan = request.MaPhongBan;
            obj.TenPhongBan = request.TenPhongBan;
            obj.ViTri = request.ViTri;
            obj.MoTa = request.MoTa;
            _context.PhongBans.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.PhongBans.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Phòng ban không tồn tại {Id}");
            _context.PhongBans.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<PhongBan> GetById(int Id)
        {
            var objdb = await _context.PhongBans.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<PhongBan> GetAllRecords()
        {
            return _context.Set<PhongBan>().AsNoTracking().ToList();
        }

    }
}