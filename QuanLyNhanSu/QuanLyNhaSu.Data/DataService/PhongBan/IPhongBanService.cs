﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IPhongBanService
    {
        Task<int> Create(PhongBanViewModel request);

        Task<int> Update(PhongBan request);

        Task<int> Delete(int Id);

        Task<PhongBan> GetById(int Id);

        IEnumerable<PhongBan> GetAllRecords();
    }
}