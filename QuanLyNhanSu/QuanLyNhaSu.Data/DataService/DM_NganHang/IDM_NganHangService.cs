﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_NganHangService
    {
        Task<int> Create(DM_NganHangViewModel request);

        Task<int> Update(DM_NganHang request);

        Task<int> Delete(int Id);

        Task<DM_NganHang> GetById(int Id);
        IEnumerable<DM_NganHang> GetAllRecords();
    }
}