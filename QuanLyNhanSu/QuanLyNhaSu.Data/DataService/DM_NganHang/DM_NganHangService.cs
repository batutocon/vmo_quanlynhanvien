﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_NganHangService : IDM_NganHangService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_NganHangService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_NganHangViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_NganHang()
            {
                MaNganHang = request.MaNganHang,
                TenNganHang = request.TenNganHang,
                TenVietTat = request.TenVietTat,
                TrangThai = request.TrangThai,
            };
            _context.DM_NganHangs.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaNganHang = "MBV00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaNganHang = "MBV0" + obj.Id;
            }
            else
            {
                obj.MaNganHang = "MBV" + obj.Id;
            }
            _context.DM_NganHangs.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_NganHang request)
        {
            var obj = await _context.DM_NganHangs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {request.Id}");
            obj.MaNganHang = request.MaNganHang;
            obj.TenNganHang = request.TenNganHang;
            obj.TenVietTat = request.TenVietTat;
            obj.TrangThai = request.TrangThai;
            _context.DM_NganHangs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_NganHangs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_NganHangs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_NganHang> GetById(int Id)
        {
            var objdb = await _context.DM_NganHangs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<DM_NganHang> GetAllRecords()
        {
            return _context.Set<DM_NganHang>().AsNoTracking().ToList();
        }

    }
}