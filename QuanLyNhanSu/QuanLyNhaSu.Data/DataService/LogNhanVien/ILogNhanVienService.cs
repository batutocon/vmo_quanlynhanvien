﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;
using System;

namespace QuanLyNhanSu.Data.DataService
{
    public interface ILogNhanVienService
    {
        IEnumerable<LogNhanVien> GetAllRecords(string TuNgay, string DenNgay, int type);
    }
}