﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Globalization;

namespace QuanLyNhanSu.Data.DataService
{
    public class LogNhanVienService : ILogNhanVienService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public LogNhanVienService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public IEnumerable<LogNhanVien> GetAllRecords(string TuNgay, string DenNgay, int type)
        {
            DateTime StartAt = !string.IsNullOrEmpty(TuNgay) ? DateTime.ParseExact(TuNgay, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;
            DateTime EndAt = !string.IsNullOrEmpty(DenNgay) ? DateTime.ParseExact(DenNgay, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;                
            TimeSpan ts = new TimeSpan(0, 0, 0);
            StartAt = StartAt.Date + ts;
            TimeSpan te = new TimeSpan(23, 59, 59);
            EndAt = EndAt.Date + te;
            List<LogNhanVien> lstObj = new List<LogNhanVien>();
            lstObj = _context.LogNhanViens.Where(x => x.TaoLuc <= EndAt && x.TaoLuc >= StartAt && x.Type == type).ToList();
            return lstObj;
        }
    }
}