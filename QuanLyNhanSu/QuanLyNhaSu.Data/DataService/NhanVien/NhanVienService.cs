﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Reflection;

namespace QuanLyNhanSu.Data.DataService
{
    public class NhanVienService : INhanVienService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public NhanVienService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(NhanVienViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.NhanVien()
            {
                Ten = request.Ten,
                GioiTinh = request.GioiTinh,
                NgaySinh = request.NgaySinh,
                DiaChi = request.DiaChi,
                CanCuoc_CMND = request.CanCuoc_CMND,
                NgayCap = request.NgayCap,
                NoiCap = request.NoiCap,
                Email = request.Email,
                ChucVu = request.ChucVu,
                PhongBan = request.PhongBan,
                TrangThai = request.TrangThai,
                NgayTao = DateTime.Now,
                NguoiTao = request.NguoiTao
            };
            _context.NhanViens.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.IdCBNV = "00000" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.IdCBNV = "0000" + obj.Id;
            }
            else if (obj.Id > 99 && obj.Id < 1000)
            {
                obj.IdCBNV = "000" + obj.Id;
            }
            else if (obj.Id > 999 && obj.Id < 10000)
            {
                obj.IdCBNV = "00" + obj.Id;
            }
            else if (obj.Id > 9999 && obj.Id < 100000)
            {
                obj.IdCBNV = "0" + obj.Id;
            }
            else
            {
                obj.IdCBNV = obj.Id.ToString();
            }
            _context.NhanViens.Update(obj);
            _context.SaveChanges();

            LogNhanVien log = new LogNhanVien();
            log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
            log.TenNhanVien = obj.Ten;
            log.MaNhanVien = obj.IdCBNV;
            log.TaoLuc = DateTime.Now;
            log.TaoBoi = request.NguoiTao;
            log.HanhDong = "Thêm mới nhân viên";
            log.Old = "~";
            log.New = request.Ten;
            _context.LogNhanViens.Add(log); 
            _context.SaveChanges();

            return obj.Id;
        }
        public async Task<int> Update(NhanVien request)
        {
            var obj = await _context.NhanViens.FindAsync(request.Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Nhân viên không tồn tại {request.Id}");

            //check trạng thái
            if (request.TrangThai == SystemConstants.Web.TRANGTHAI_NHANVIEN_NGHIVIEC && obj.TrangThai != request.TrangThai)
            {
                var hd = _context.LichSuHopDongs.FirstOrDefault(x=>x.IdNhanVien == obj.Id);
                if (hd!=null) {
                    hd.TrangThai = SystemConstants.Web.TRANGTHAI_HOPDONG_HUYHOPDONG;
                    _context.LichSuHopDongs.Update(hd);
                }
                var nghiviec = new QLNghiViec()
                {
                    IdNhanVien = obj.Id,
                    Note = "",
                    TaoLuc = DateTime.Now,
                    TaoBoi = request.NguoiTao,
                    TrangThai = SystemConstants.Web.TRANGTHAI_NGHIVIEC_CHOPHEDUYET,
                };
                _context.QLNghiViecs.Add(nghiviec);
            }
            // check tên
            if (!request.Ten.Equals(obj.Ten))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin tên";
                log.Old = obj.Ten;
                log.New = request.Ten;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check giới tính
            if (!request.GioiTinh.Equals(obj.GioiTinh))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin giới tính";
                log.Old = obj.GioiTinh;
                log.New = request.GioiTinh;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check ngày sinh
            if (DateTime.Compare(obj.NgaySinh, request.NgaySinh)!=0)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin ngày sinh";
                log.Old = obj.NgaySinh.ToString("MM/dd/yyyy");
                log.New = request.NgaySinh.ToString("MM/dd/yyyy");
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check địa chỉ
            if (!request.DiaChi.Equals(obj.DiaChi))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin địa chỉ";
                log.Old = obj.DiaChi;
                log.New = request.DiaChi;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check căn cước
            if (!request.CanCuoc_CMND.Equals(obj.CanCuoc_CMND))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin CMND";
                log.Old = obj.CanCuoc_CMND;
                log.New = request.CanCuoc_CMND;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check ngày cấp
            if (DateTime.Compare(obj.NgayCap, request.NgayCap) != 0)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin ngày cấp";
                log.Old = obj.NgayCap.ToString("MM/dd/yyyy");
                log.New = request.NgayCap.ToString("MM/dd/yyyy");
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check nơi cấp
            if (!request.NoiCap.Equals(obj.NoiCap))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin nơi cấp";
                log.Old = obj.NoiCap;
                log.New = request.NoiCap;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check mail
            if (!request.Email.Equals(obj.Email))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin nơi Email";
                log.Old = obj.Email;
                log.New = request.Email;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check phòng ban
            if (request.PhongBan != obj.PhongBan)
            {
                var oldPB = _context.PhongBans.FirstOrDefault(x => x.Id == obj.PhongBan);
                var newPB = _context.PhongBans.FirstOrDefault(x => x.Id == request.PhongBan);
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin Phòng ban";
                log.Old = oldPB != null ? oldPB.TenPhongBan : "N/A"; ;
                log.New = newPB != null ? newPB.TenPhongBan : "N/A"; ;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();
            }
            // check chức danh
            if (request.ChucVu != obj.ChucVu)
            {
                var oldchuvu = _context.DM_ChucDanhs.FirstOrDefault(x => x.Id == obj.ChucVu);
                var newchuvu = _context.DM_ChucDanhs.FirstOrDefault(x => x.Id == request.ChucVu);
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_NHANVIEN;
                log.TenNhanVien = obj.Ten;
                log.MaNhanVien = obj.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin Chức danh";
                log.Old = oldchuvu != null ? oldchuvu.TenChucDanh : "N/A"; ;
                log.New = newchuvu != null ? newchuvu.TenChucDanh : "N/A"; ;
                _context.LogNhanViens.Add(log);            _context.SaveChanges();

            }


            obj.Ten = request.Ten;
            obj.GioiTinh = request.GioiTinh;
            obj.NgaySinh = request.NgaySinh;
            obj.DiaChi = request.DiaChi;
            obj.CanCuoc_CMND = request.CanCuoc_CMND;
            obj.NgayCap = request.NgayCap;
            obj.NoiCap = request.NoiCap;
            obj.Email = request.Email;
            obj.ChucVu = request.ChucVu;
            obj.PhongBan = request.PhongBan;
            obj.TrangThai = request.TrangThai;
            _context.NhanViens.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.NhanViens.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Nhân viên không tồn tại {Id}");
            _context.NhanViens.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<NhanVien> GetById(int Id)
        {
            var objdb = await _context.NhanViens.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<ViewModel.ViewData.NhanVienViewModel> GetAllRecords()
        {
            List<ViewModel.ViewData.NhanVienViewModel> lstObj = new List<ViewModel.ViewData.NhanVienViewModel>();
            var lstnv = _context.NhanViens.Where(x => x.TrangThai == 1).AsNoTracking().ToList();
            try
            {
                foreach (var itemNV in lstnv)
                {
                    ViewModel.ViewData.NhanVienViewModel obj = new ViewModel.ViewData.NhanVienViewModel();
                    var chucdanh = _context.DM_ChucDanhs.FirstOrDefault(x => x.Id == itemNV.ChucVu);
                    var phongban = _context.PhongBans.FirstOrDefault(x => x.Id == itemNV.PhongBan);
                    obj.nhanvien = itemNV;
                    obj.chucdanh = chucdanh;
                    obj.phongban = phongban;
                    lstObj.Add(obj);
                }
            }
            catch(Exception ex)
            {
                return null;
            }
            return lstObj;
        }

    }
}