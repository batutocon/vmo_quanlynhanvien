﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface INhanVienService
    {
        Task<int> Create(NhanVienViewModel request);

        Task<int> Update(NhanVien request);

        Task<int> Delete(int Id);

        Task<NhanVien> GetById(int Id);

        IEnumerable<ViewModel.ViewData.NhanVienViewModel> GetAllRecords();
    }
}