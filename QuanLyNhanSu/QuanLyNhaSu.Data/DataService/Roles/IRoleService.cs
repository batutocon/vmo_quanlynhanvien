﻿using QuanLyNhanSu.Data.ViewModel.Roles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IRoleService
    {
        Task<List<RoleVm>> GetAll();
    }
}