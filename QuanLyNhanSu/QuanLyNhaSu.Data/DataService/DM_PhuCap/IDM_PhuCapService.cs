﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_PhuCapService
    {
        Task<int> Create(DM_PhuCapViewModel request);

        Task<int> Update(DM_PhuCap request);

        Task<int> Delete(int Id);

        Task<DM_PhuCap> GetById(int Id);
        IEnumerable<DM_PhuCap> GetAllRecords();
    }
}