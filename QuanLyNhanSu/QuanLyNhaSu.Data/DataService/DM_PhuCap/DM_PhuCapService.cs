﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_PhuCapService : IDM_PhuCapService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_PhuCapService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_PhuCapViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_PhuCap()
            {
                MaPhuCap = request.MaPhuCap,
                LoaiThuong = request.LoaiThuong,
                NguonChi = request.NguonChi,
                SoTienTheoLuongCB = request.SoTienTheoLuongCB,
                TrangThai = request.TrangThai,
                GhiChu = request.GhiChu,
            };
            _context.DM_PhuCaps.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaPhuCap = "MBV00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaPhuCap = "MBV0" + obj.Id;
            }
            else
            {
                obj.MaPhuCap = "MBV" + obj.Id;
            }
            _context.DM_PhuCaps.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_PhuCap request)
        {
            var obj = await _context.DM_PhuCaps.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {request.Id}");
            obj.MaPhuCap = request.MaPhuCap;
            obj.LoaiThuong = request.LoaiThuong;
            obj.NguonChi = request.NguonChi;
            obj.SoTienTheoLuongCB = request.SoTienTheoLuongCB;
            obj.TrangThai = request.TrangThai;
            obj.GhiChu = request.GhiChu;
            _context.DM_PhuCaps.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_PhuCaps.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_PhuCaps.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_PhuCap> GetById(int Id)
        {
            var objdb = await _context.DM_PhuCaps.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<DM_PhuCap> GetAllRecords()
        {
            return _context.Set<DM_PhuCap>().AsNoTracking().ToList();
        }

    }
}