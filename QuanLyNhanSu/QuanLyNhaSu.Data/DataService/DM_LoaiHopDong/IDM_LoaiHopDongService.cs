﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_LoaiHopDongService
    {
        Task<int> Create(DM_LoaiHopDongViewModel request);

        Task<int> Update(DM_LoaiHopDong request);

        Task<int> Delete(int Id);

        Task<DM_LoaiHopDong> GetById(int Id);

        IEnumerable<DM_LoaiHopDong> GetAllRecords();
    }
}