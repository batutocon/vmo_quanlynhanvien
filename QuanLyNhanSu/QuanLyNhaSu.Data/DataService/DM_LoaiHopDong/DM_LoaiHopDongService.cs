﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_LoaiHopDongService : IDM_LoaiHopDongService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_LoaiHopDongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_LoaiHopDongViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_LoaiHopDong()
            {
                MaLoaiHopDong = request.MaLoaiHopDong,
                TenLoaiHopDong = request.TenLoaiHopDong,
                ThoiHanHopDong = request.ThoiHanHopDong,
                TrangThai = request.TrangThai,
                GhiChu = request.GhiChu,
            };
            _context.DM_LoaiHopDongs.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaLoaiHopDong = "MBV00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaLoaiHopDong = "MBV0" + obj.Id;
            }
            else
            {
                obj.MaLoaiHopDong = "MBV" + obj.Id;
            }
            _context.DM_LoaiHopDongs.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_LoaiHopDong request)
        {
            var obj = await _context.DM_LoaiHopDongs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {request.Id}");
            obj.MaLoaiHopDong = request.MaLoaiHopDong;
            obj.TenLoaiHopDong = request.TenLoaiHopDong;
            obj.ThoiHanHopDong = request.ThoiHanHopDong;
            obj.TrangThai = request.TrangThai;
            obj.GhiChu = request.GhiChu;
            _context.DM_LoaiHopDongs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_LoaiHopDongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_LoaiHopDongs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_LoaiHopDong> GetById(int Id)
        {
            var objdb = await _context.DM_LoaiHopDongs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DM_LoaiHopDong> GetAllRecords()
        {
            return _context.Set<DM_LoaiHopDong>().AsNoTracking().ToList();
        }


    }
}