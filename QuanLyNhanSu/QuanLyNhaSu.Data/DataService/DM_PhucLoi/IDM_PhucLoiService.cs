﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_PhucLoiService
    {
        Task<int> Create(DM_PhucLoiViewModel request);

        Task<int> Update(DM_PhucLoi request);

        Task<int> Delete(int Id);

        Task<DM_PhucLoi> GetById(int Id);

        IEnumerable<DM_PhucLoi> GetAllRecords();
    }
}