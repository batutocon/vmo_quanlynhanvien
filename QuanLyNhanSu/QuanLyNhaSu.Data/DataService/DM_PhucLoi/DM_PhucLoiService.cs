﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_PhucLoiService : IDM_PhucLoiService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_PhucLoiService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_PhucLoiViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_PhucLoi()
            {
                MaCheDoPhucLoi = request.MaCheDoPhucLoi,
                SoTienTruocThue = request.SoTienTruocThue,
                NgayHieuLuc = request.NgayHieuLuc,
                NgayHetHieuLuc = request.NgayHetHieuLuc,
                NguonChi = request.NguonChi,
                TrangThai = request.TrangThai,
                MucChiToiDa = request.MucChiToiDa,
                TenPhucLoi = request.TenPhucLoi
            };
            _context.DM_PhucLois.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaCheDoPhucLoi = "MPL00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaCheDoPhucLoi = "MPL0" + obj.Id;
            }
            else
            {
                obj.MaCheDoPhucLoi = "MPL" + obj.Id;
            }
            _context.DM_PhucLois.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_PhucLoi request)
        {
            var obj = await _context.DM_PhucLois.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục phúc lợi không tồn tại {request.Id}");
            obj.MaCheDoPhucLoi = request.MaCheDoPhucLoi;
            obj.SoTienTruocThue = request.SoTienTruocThue;
            obj.NgayHieuLuc = request.NgayHieuLuc;
            obj.NgayHetHieuLuc = request.NgayHetHieuLuc;
            obj.NguonChi = request.NguonChi;
            obj.TrangThai = request.TrangThai;
            obj.MucChiToiDa = request.MucChiToiDa;
            obj.TenPhucLoi = request.TenPhucLoi;
            _context.DM_PhucLois.Update(obj);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_PhucLois.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục phúc lợi không tồn tại {Id}");
            _context.DM_PhucLois.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_PhucLoi> GetById(int Id)
        {
            var objdb = await _context.DM_PhucLois.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<DM_PhucLoi> GetAllRecords()
        {
            return _context.Set<DM_PhucLoi>().AsNoTracking().ToList();
        }
    }
}