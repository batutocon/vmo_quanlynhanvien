﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using Trading.CMS.Repository;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using QuanLyNhanSu.Common.Constants;

namespace QuanLyNhanSu.Data.DataService
{
    public class BangLuongService : IBangLuongService
    {
        private readonly QuanLyNhanSuDbContext _context;
        private static GenericUnitOfWork _unitOfWork;
        public BangLuongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
            _unitOfWork = new GenericUnitOfWork(context);
        }

        public async Task<int> Create(BangLuongViewModel request)
        {
            var obj = new BangLuong();
            obj.IdLichSuHopDong = request.IdLichSuHopDong;
            obj.IdNhanVien = request.IdNhanVien;
            obj.Nam = request.Nam;
            obj.Thang = request.Thang;
            obj.NgayTao = DateTime.Now;
            obj.NgayNhanLuong = DateTime.Now;
            obj.NguoiTao = request.NguoiTao;
            obj.TrangThai = request.TrangThai;
            _context.BangLuongs.Add(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> UpdateTinhLuong(int Id, double luongCB, int heso)
        {
            var obj = await _context.BangLuongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Không tồn tại {Id}");
            double tienphucap = 0, tienphucloi = 0;
            List<DM_PhuCap> lstPC = _context.DM_PhuCaps.Where(x => x.TrangThai == 1).AsNoTracking().ToList();
            if (lstPC.Count > 0)
            {
                foreach(var item in lstPC)
                {
                    tienphucap += luongCB * heso * item.SoTienTheoLuongCB / 100;
                }
            }
            List<DM_PhucLoi> lstPL = _context.DM_PhucLois.Where(x=>x.TrangThai == 1).AsNoTracking().ToList();
            if (lstPL.Count > 0)
            {
                foreach (var item in lstPL)
                {
                    double tien = 0;
                    double.TryParse(item.SoTienTruocThue, out tien);
                    tienphucloi += tien;
                }
            }
            obj.TienPhuCap = tienphucap;
            obj.TienPhucLoi = tienphucloi;
            obj.TongLuong = luongCB * heso + obj.TienPhuCap+ obj.TienPhucLoi;
            obj.TrangThai = SystemConstants.Web.TRANGTHAI_BANGLUONG_TAMTINHLUONG;
            _context.BangLuongs.Update(obj);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> UpdateThuongPhat(int Id)
        {
            double tienthuong = 0, tienphat = 0;
            var obj = await _context.BangLuongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Không tồn tại {Id}");
            var lstThuong = _context.KhenThuongs.Where(x => x.Thang == obj.Thang && x.Nam == obj.Nam && x.IdNhanVien == obj.IdNhanVien).ToList();
            if (lstThuong.Count() > 0)
            {
                foreach(var item in lstThuong)
                {
                    tienthuong += item.SoTien;
                }
            }
            var lstPhat = _context.KyLuats.Where(x => x.Thang == obj.Thang && x.Nam == obj.Nam && x.IdNhanVien == obj.IdNhanVien).ToList();
            if (lstPhat.Count() > 0)
            {
                foreach (var item in lstPhat)
                {
                    tienphat += item.SoTien;
                }
            }

            obj.TongLuong = obj.TongLuong - obj.TienThuong + tienthuong + obj.TienPhat - tienphat;
            obj.TienThuong = tienthuong;
            obj.TienPhat = tienphat;
            obj.TrangThai = SystemConstants.Web.TRANGTHAI_BANGLUONG_TAMTINHLUONG;
            _context.BangLuongs.Update(obj);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> UpdateNhanLuong(int id,int trangthai)
        {
            var obj = await _context.BangLuongs.FindAsync(id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Không tồn tại {id}");
            obj.NgayNhanLuong = DateTime.Now;
            obj.TrangThai = trangthai;
            _context.BangLuongs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.BangLuongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.BangLuongs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<BangLuong> GetById(int Id)
        {
            var objdb = await _context.BangLuongs.FindAsync(Id);
            if(objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }
            
        }
        public IEnumerable<BangLuong> GetAllRecords()
        {
            return _context.Set<BangLuong>().AsNoTracking().ToList();
        }
        public IEnumerable<BangLuongTheoThangNam> GetListBangLuongTheoThangNam(int Thang, int Nam)
        {
            var lstobj = new List<BangLuongTheoThangNam>();
            var lstnv = _context.NhanViens.Where(x => x.TrangThai == 1).ToList();
            foreach(var itemNV in lstnv)
            {
                //nhanvien
                var obj = new BangLuongTheoThangNam();
                obj.nhanvien = itemNV;

                //hopdong hệ số lương
                LichSuHopDong hd = new LichSuHopDong();
                HeSoLuong hsl = new HeSoLuong();
                BangLuong bangluong = new BangLuong();

                var hd_db = _context.LichSuHopDongs.FirstOrDefault(x => x.IdNhanVien == itemNV.Id && x.TrangThai == 1);
                if (hd_db != null)
                {
                    obj.hopdong = hd_db;
                    hsl = _context.HeSoLuongs.FirstOrDefault(x => x.Id == obj.hopdong.IdHeSoLuong && x.TrangThai == 1);
                    obj.hesoluong = hsl;
                    //bangluong
                    var bangluong_db = _context.BangLuongs.FirstOrDefault(x => x.IdNhanVien == itemNV.Id && x.Thang == Thang && x.Nam == Nam);
                    if (bangluong_db != null)
                    {
                        obj.bangluong = bangluong_db;
                    }
                    else
                    {
                        obj.bangluong = bangluong;
                    }
                }
                else
                {
                    obj.hesoluong = hsl;
                    obj.hopdong = hd;
                    obj.bangluong = bangluong;
                }
                lstobj.Add(obj);
            }
            return lstobj;
        }
    }
}