﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IBangLuongService
    {
        Task<int> Create(BangLuongViewModel request);

        Task<int> UpdateTinhLuong(int Id, double luongCB, int heso);

        Task<int> UpdateThuongPhat(int Id);
        Task<int> UpdateNhanLuong(int id, int trangthai);

        Task<int> Delete(int Id);

        Task<BangLuong> GetById(int Id);

        IEnumerable<BangLuong> GetAllRecords();

        IEnumerable<BangLuongTheoThangNam> GetListBangLuongTheoThangNam(int Thang,int Nam);
    }
}