﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace QuanLyNhanSu.Data.DataService
{
    public class QLNghiViecService : IQLNghiViecService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public QLNghiViecService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Update(int Id, int trangthai, string note)
        {
            var obj = await _context.QLNghiViecs.FindAsync(Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Nhân viên không tồn tại {Id}");

            obj.TrangThai = trangthai;
            obj.Note = note;

            _context.QLNghiViecs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.QLNghiViecs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Nhân viên không tồn tại {Id}");
            _context.QLNghiViecs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<QLNghiViec> GetById(int Id)
        {
            var objdb = await _context.QLNghiViecs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<QLNghiViecGetAllViewModel> GetAllRecords()
        {
            List<QLNghiViecGetAllViewModel> lstObj = new List<QLNghiViecGetAllViewModel>();
            var lstNghiViec = _context.QLNghiViecs.AsNoTracking().ToList();
            try
            {
                foreach (var itemNghiViec in lstNghiViec)
                {
                    QLNghiViecGetAllViewModel obj = new QLNghiViecGetAllViewModel();
                    NhanVien nv = _context.NhanViens.FirstOrDefault(x => x.Id == itemNghiViec.IdNhanVien);
                    obj.qlnghiviec = itemNghiViec;
                    obj.nhanVien = nv;
                    var nv_tscp = _context.Set<NhanVien_TSCP>().Where(x => x.IdNhanVien == itemNghiViec.IdNhanVien).FirstOrDefault();
                    if (nv_tscp != null)
                    {
                        obj.nhanVien_TSCP = nv_tscp;
                        obj.dM_TaiSanCapPhats = new List<DM_TaiSanCapPhat>();
                        string[] listTSCP = nv_tscp.IdTSCP.Split(";");
                        foreach (string itemStr in listTSCP)
                        {
                            string[] child_item = itemStr.Split(":");
                            var tscp = _context.Set<DM_TaiSanCapPhat>().Where(x => x.MaTaiSanCapPhat == child_item[0]).FirstOrDefault();
                            if (tscp != null)
                            {
                                int soluong = 1;
                                int.TryParse(child_item[1], out soluong);
                                tscp.SoLuong = soluong;
                                obj.dM_TaiSanCapPhats.Add(tscp);
                            }
                        }
                    }
                    else
                    {
                        obj.nhanVien_TSCP = new NhanVien_TSCP();
                        obj.dM_TaiSanCapPhats = new List<DM_TaiSanCapPhat>();
                    }
                    lstObj.Add(obj);
                }
            }
            catch(Exception ex)
            {
                return null;
            }
            return lstObj;
        }

    }
}