﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IQLNghiViecService
    {
        Task<int> Update(int Id, int trangthai, string note);

        Task<int> Delete(int Id);

        Task<QLNghiViec> GetById(int Id);

        IEnumerable<QLNghiViecGetAllViewModel> GetAllRecords();
    }
}