﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace QuanLyNhanSu.Data.DataService
{
    public class KyLuatService : IKyLuatService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public KyLuatService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(KyLuatViewModel request)
        {
            var obj = new KyLuat()
            {
                IdNhanVien = request.IdNhanVien,
                Nam = request.Nam,
                Thang = request.Thang,
                NgayTao = DateTime.Now,
                NguoiTao = request.NguoiTao,
                NoiDungKyLuat = request.NoiDungKyLuat,
                SoTien = request.SoTien,
            };
            _context.KyLuats.Add(obj);
            await _context.SaveChangesAsync();
            return obj.Id;
        }
        //public async Task<int> Update(PhongBan request)
        //{
        //    var obj = await _context.PhongBans.FindAsync(request.Id);

        //    if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Phòng ban không tồn tại {request.Id}");
        //    obj.MaPhongBan = request.MaPhongBan;
        //    obj.TenPhongBan = request.TenPhongBan;
        //    obj.ViTri = request.ViTri;
        //    obj.MoTa = request.MoTa;
        //    _context.PhongBans.Update(obj);
        //    return await _context.SaveChangesAsync();
        //}

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.KyLuats.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Không tồn tại {Id}");
            _context.KyLuats.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<KyLuat> GetById(int Id)
        {
            var objdb = await _context.KyLuats.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<KyLuatViewDetailModel> GetListKyLuat(int Thang, int Nam)
        {
            List<KyLuatViewDetailModel> lst = new List<KyLuatViewDetailModel>();
            List<NhanVien> lstNV = _context.NhanViens.Where(x => x.TrangThai == 1).AsNoTracking().ToList();
            foreach (var item in lstNV)
            {
                KyLuatViewDetailModel obj = new KyLuatViewDetailModel();
                obj.nhanVien = item;
                var lstKyLuat = _context.Set<KyLuat>().Where(x => x.IdNhanVien == item.Id && x.Thang == Thang && x.Nam == Nam).AsNoTracking().ToList();
                obj.KyLuats = lstKyLuat;
                lst.Add(obj);
            }
            return lst;
        }
        public KyLuatViewDetailModel GetDetailKyLuatByNhanVien(int IdNhanVien, int Thang, int Nam)
        {
            KyLuatViewDetailModel obj = new KyLuatViewDetailModel();
            NhanVien item = _context.Set<NhanVien>().Where(x => x.Id == IdNhanVien).FirstOrDefault();
            obj.nhanVien = item;
            var lstKyLuat = _context.Set<KyLuat>().Where(x => x.IdNhanVien == item.Id && x.Thang == Thang && x.Nam == Nam).AsNoTracking().ToList();
            obj.KyLuats = lstKyLuat;
            return obj;
        }
    }
}