﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IKyLuatService
    {
        Task<int> Create(KyLuatViewModel request);

        //Task<int> Update(PhongBan request);

        Task<int> Delete(int Id);

        Task<KyLuat> GetById(int Id);

        IEnumerable<KyLuatViewDetailModel> GetListKyLuat(int Thang, int Nam);
        KyLuatViewDetailModel GetDetailKyLuatByNhanVien(int IdNhanVien, int Thang, int Nam);
    }
}