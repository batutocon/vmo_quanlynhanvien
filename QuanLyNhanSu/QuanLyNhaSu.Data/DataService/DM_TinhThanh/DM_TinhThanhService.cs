﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.ViewModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_TinhThanhService : IDM_TinhThanhService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_TinhThanhService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_TinhThanhViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_TinhThanh()
            {
                MaQuocGia = request.MaQuocGia,
                MaTinhThanh = request.MaTinhThanh,
                TenTinhThanh = request.TenTinhThanh,
                TrangThai = request.TrangThai,
            };
            _context.DM_TinhThanhs.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaTinhThanh = "MTT00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaTinhThanh = "MTT0" + obj.Id;
            }
            else
            {
                obj.MaTinhThanh = "MTT" + obj.Id;
            }
            _context.DM_TinhThanhs.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_TinhThanh request)
        {
            var obj = await _context.DM_TinhThanhs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục tham số hệ thống không tồn tại {request.Id}");
            obj.MaQuocGia = request.MaQuocGia;
            obj.MaTinhThanh = request.MaTinhThanh;
            obj.TenTinhThanh = request.TenTinhThanh;
            obj.TrangThai = request.TrangThai;
            _context.DM_TinhThanhs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_TinhThanhs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_TinhThanhs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_TinhThanh> GetById(int Id)
        {
            var objdb = await _context.DM_TinhThanhs.FindAsync(Id);
            if (objdb != null)
            {       
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DM_TinhThanh> GetAllRecords()
        {
            return _context.Set<DM_TinhThanh>().AsNoTracking().ToList();
        }

        public IEnumerable<DM_TinhThanh> GetListTinhThanhByQuocGia(string MaQuocGia)
        {
            return _context.Set<DM_TinhThanh>().Where(x => x.MaQuocGia == MaQuocGia).ToList();
        }
    }
}