﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_TinhThanhService
    {
        Task<int> Create(DM_TinhThanhViewModel request);

        Task<int> Update(DM_TinhThanh request);

        Task<int> Delete(int Id);

        Task<DM_TinhThanh> GetById(int Id);

        IEnumerable<DM_TinhThanh> GetAllRecords();

        IEnumerable<DM_TinhThanh> GetListTinhThanhByQuocGia(string MaQuocGia);

    }
}