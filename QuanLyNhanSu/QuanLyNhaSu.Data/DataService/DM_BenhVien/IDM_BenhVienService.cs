﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_BenhVienService
    {
        Task<int> Create(DM_BenhVienViewModel request);

        Task<int> Update(DM_BenhVien request);

        Task<int> Delete(int Id);

        Task<DM_BenhVien> GetById(int Id);

        IEnumerable<DM_BenhVien> GetAllRecords();
    }
}