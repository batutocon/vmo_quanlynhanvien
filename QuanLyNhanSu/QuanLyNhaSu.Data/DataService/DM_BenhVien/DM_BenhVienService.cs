﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using Trading.CMS.Repository;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_BenhVienService : IDM_BenhVienService
    {
        private readonly QuanLyNhanSuDbContext _context;
        private static GenericUnitOfWork _unitOfWork;
        public DM_BenhVienService(QuanLyNhanSuDbContext context)
        {
            _context = context;
            _unitOfWork = new GenericUnitOfWork(context);
        }

        public async Task<int> Create(DM_BenhVienViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_BenhVien()
            {
                MaBenhVien = request.MaBenhVien,
                TenBenhVien = request.TenBenhVien,
                DiaChi = request.DiaChi,
                MoTa = request.MoTa,
            };
            _context.DM_BenhViens.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaBenhVien = "MBV00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaBenhVien = "MBV0" + obj.Id;
            }
            else
            {
                obj.MaBenhVien = "MBV" + obj.Id;
            }
            _context.DM_BenhViens.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_BenhVien request)
        {
            var obj = await _context.DM_BenhViens.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {request.Id}");
            obj.MaBenhVien = request.MaBenhVien;
            obj.TenBenhVien = request.TenBenhVien;
            obj.DiaChi = request.DiaChi;
            obj.MoTa = request.MoTa;
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_BenhViens.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_BenhViens.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_BenhVien> GetById(int Id)
        {
            var objdb = await _context.DM_BenhViens.FindAsync(Id);
            if(objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }
            
        }
        public IEnumerable<DM_BenhVien> GetAllRecords()
        {
            return _context.Set<DM_BenhVien>().AsNoTracking().ToList();
        }
    }
}