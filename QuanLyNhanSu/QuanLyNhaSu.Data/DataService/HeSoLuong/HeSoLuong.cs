﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class HeSoLuongService : IHeSoLuongService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public HeSoLuongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(HeSoLuongViewModel request)
        {
            var obj = new HeSoLuong()
            {
                Ten = request.Ten,
                HeSo = request.HeSo,
                NgayTao = request.NgayTao,
                NguoiTao = request.NguoiTao,
                TrangThai = request.TrangThai
            };
            _context.HeSoLuongs.Add(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(HeSoLuong request)
        {
            var obj = await _context.HeSoLuongs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục hệ số lương không tồn tại {request.Id}");
            obj.Ten = request.Ten;
            obj.HeSo = request.HeSo;
            obj.NgayTao = request.NgayTao;
            obj.NguoiTao = request.NguoiTao;
            obj.TrangThai = request.TrangThai;
            _context.HeSoLuongs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.HeSoLuongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục hệ số lương không tồn tại {Id}");
            _context.HeSoLuongs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<HeSoLuong> GetById(int Id)
        {
            var objdb = await _context.HeSoLuongs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<HeSoLuong> GetAllRecords()
        {
            return _context.Set<HeSoLuong>().AsNoTracking().ToList();
        }
    }
}