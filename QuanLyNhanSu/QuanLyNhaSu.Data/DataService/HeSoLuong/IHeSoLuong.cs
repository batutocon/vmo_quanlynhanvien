﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IHeSoLuongService
    {
        Task<int> Create(HeSoLuongViewModel request);

        Task<int> Update(HeSoLuong request);

        Task<int> Delete(int Id);

        Task<HeSoLuong> GetById(int Id);
        IEnumerable<HeSoLuong> GetAllRecords();
    }
}