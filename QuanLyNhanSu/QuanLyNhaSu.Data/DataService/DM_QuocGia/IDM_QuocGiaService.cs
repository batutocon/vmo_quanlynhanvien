﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_QuocGiaService
    {
        Task<int> Create(DM_QuocGiaViewModel request);

        Task<int> Update(DM_QuocGia request);

        Task<int> Delete(int Id);

        Task<DM_QuocGia> GetById(int Id);

        IEnumerable<DM_QuocGia> GetAllRecords();
    }
}