﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_QuocGiaService : IDM_QuocGiaService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_QuocGiaService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_QuocGiaViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_QuocGia()
            {
                MaQuocGia = request.MaQuocGia,
                TenQuocGia = request.TenQuocGia,
                TrangThai = request.TrangThai,
              
            };
            _context.DM_QuocGias.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaQuocGia = "MQG00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaQuocGia = "MQG0" + obj.Id;
            }
            else
            {
                obj.MaQuocGia = "MQG" + obj.Id;
            }
            _context.DM_QuocGias.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_QuocGia request)
        {
            var obj = await _context.DM_QuocGias.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục quốc gia không tồn tại {request.Id}");
            obj.MaQuocGia = request.MaQuocGia;
            obj.TenQuocGia = request.TenQuocGia;
            obj.TrangThai = request.TrangThai;
            _context.DM_QuocGias.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_QuocGias.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục quốc gia không tồn tại {Id}");
            _context.DM_QuocGias.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_QuocGia> GetById(int Id)
        {
            var objdb = await _context.DM_QuocGias.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<DM_QuocGia> GetAllRecords()
        {
            return _context.Set<DM_QuocGia>().AsNoTracking().ToList();
        }

    }
}