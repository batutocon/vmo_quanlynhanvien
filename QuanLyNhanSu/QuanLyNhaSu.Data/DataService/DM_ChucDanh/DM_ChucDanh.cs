﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_ChucDanhService : IDM_ChucDanhService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_ChucDanhService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_ChucDanhViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_ChucDanh()
            {
                MaChucDanh = request.MaChucDanh,
                TenChucDanh = request.TenChucDanh,
                IdDonVi = request.IdDonVi,
                MoTa = request.MoTa,
            };
            _context.DM_ChucDanhs.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaChucDanh = "MCD00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaChucDanh = "MCD0" + obj.Id;
            }
            else
            {
                obj.MaChucDanh = "MCD" + obj.Id;
            }
            _context.DM_ChucDanhs.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_ChucDanh request)
        {
            var obj = await _context.DM_ChucDanhs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục chức danh không tồn tại {request.Id}");
            obj.MaChucDanh = request.MaChucDanh;
            obj.TenChucDanh = request.TenChucDanh;
            obj.IdDonVi = request.IdDonVi;
            obj.MoTa = request.MoTa;
            _context.DM_ChucDanhs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_ChucDanhs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục chức danh không tồn tại {Id}");
            _context.DM_ChucDanhs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_ChucDanh> GetById(int Id)
        {
            var objdb = await _context.DM_ChucDanhs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DonVi_ChucDanhViewModel> GetAllRecords()
        {
            List<DonVi_ChucDanhViewModel> lstObj = new List<DonVi_ChucDanhViewModel>();
            var lstchucdanh = _context.Set<DM_ChucDanh>().AsNoTracking().ToList();
            foreach (var item in lstchucdanh)
            {
                var donvi = _context.DonVis.FirstOrDefault(x=>x.MaDonVi.Equals(item.IdDonVi)==true);
                DonVi_ChucDanhViewModel obj = new DonVi_ChucDanhViewModel();
                obj.ChucDanh = item;
                obj.DonVi = donvi;
                lstObj.Add(obj);
            }
            return lstObj;
        }


    }
}