﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_ChucDanhService
    {
        Task<int> Create(DM_ChucDanhViewModel request);

        Task<int> Update(DM_ChucDanh request);

        Task<int> Delete(int Id);

        Task<DM_ChucDanh> GetById(int Id);
        IEnumerable<DonVi_ChucDanhViewModel> GetAllRecords();
    }
}