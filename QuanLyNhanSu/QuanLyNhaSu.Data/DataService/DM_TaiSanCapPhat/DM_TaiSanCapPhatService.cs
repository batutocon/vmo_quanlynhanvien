﻿using Microsoft.EntityFrameworkCore;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_TaiSanCapPhatService : IDM_TaiSanCapPhatService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_TaiSanCapPhatService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_TaiSanCapPhatViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_TaiSanCapPhat()
            {
                MaTaiSanCapPhat = request.MaTaiSanCapPhat,
                TenTaiSanCapPhat = request.TenTaiSanCapPhat,
                SoLuong = request.SoLuong,
                MoTa = request.MoTa,
            };
            _context.DM_TaiSanCapPhats.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaTaiSanCapPhat = "MTSCP00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaTaiSanCapPhat = "MTSCP0" + obj.Id;
            }
            else
            {
                obj.MaTaiSanCapPhat = "MTSCP" + obj.Id;
            }
            _context.DM_TaiSanCapPhats.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_TaiSanCapPhat request)
        {
            var obj = await _context.DM_TaiSanCapPhats.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục tài sản cấp phát không tồn tại {request.Id}");
            obj.MaTaiSanCapPhat = request.MaTaiSanCapPhat;
            obj.TenTaiSanCapPhat = request.TenTaiSanCapPhat;
            obj.SoLuong = request.SoLuong;
            obj.MoTa = request.MoTa;
            _context.DM_TaiSanCapPhats.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_TaiSanCapPhats.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục tài sản cấp phát không tồn tại {Id}");
            _context.DM_TaiSanCapPhats.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_TaiSanCapPhat> GetById(int Id)
        {
            var objdb = await _context.DM_TaiSanCapPhats.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DM_TaiSanCapPhat> GetAllRecords()
        {
            return _context.Set<DM_TaiSanCapPhat>().AsNoTracking().ToList();
        }
    }
}