﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_TaiSanCapPhatService
    {
        Task<int> Create(DM_TaiSanCapPhatViewModel request);

        Task<int> Update(DM_TaiSanCapPhat request);

        Task<int> Delete(int Id);

        Task<DM_TaiSanCapPhat> GetById(int Id);
        IEnumerable<DM_TaiSanCapPhat> GetAllRecords();
    }
}