﻿using QuanLyNhanSu.Data.ViewModel.Users;
using QuanLyNhanSu.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.ViewModel.Common;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IUserService
    {
        Task<ApiResult<string>> Authencate(LoginRequest request);
        Task<ApiResult<bool>> Register(RegisterRequest request);
        Task<ApiResult<UserVm>> GetById(Guid id);
        Task<ApiResult<bool>> Delete(Guid id);

    }
}