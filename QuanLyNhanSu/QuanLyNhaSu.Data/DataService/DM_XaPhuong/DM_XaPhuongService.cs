﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.ViewModel;
using System.Linq;
using System.Collections.Generic;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_XaPhuongService : IDM_XaPhuongService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_XaPhuongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_XaPhuongViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_XaPhuong()
            {
                MaXaPhuong = request.MaXaPhuong,
                TenXaPhuong = request.TenXaPhuong,
                MaQuanHuyen = request.MaQuanHuyen,
                TrangThai = request.TrangThai,
                GhiChu = request.GhiChu,

            };
            _context.DM_XaPhuongs.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaXaPhuong = "MXP00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaXaPhuong = "MXP0" + obj.Id;
            }
            else
            {
                obj.MaXaPhuong = "MXP" + obj.Id;
            }
            _context.DM_XaPhuongs.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_XaPhuong request)
        {
            var obj = await _context.DM_XaPhuongs.FindAsync(request.Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục tham số hệ thống không tồn tại {request.Id}");
            obj.MaXaPhuong = request.MaXaPhuong;
            obj.TenXaPhuong = request.TenXaPhuong;
            obj.MaQuanHuyen = request.MaQuanHuyen;
            obj.TrangThai = request.TrangThai;
            obj.GhiChu = request.GhiChu;
            _context.DM_XaPhuongs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_XaPhuongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_XaPhuongs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_XaPhuong> GetById(int Id)
        {
            var objdb = await _context.DM_XaPhuongs.FindAsync(Id);
            if (objdb != null)
            {       
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DM_XaPhuong> GetListXaPhuongByQuanHuyen(string MaQuanHuyen)
        {
            return _context.Set<DM_XaPhuong>().Where(x => x.MaQuanHuyen == MaQuanHuyen).ToList();
        }
    }
}