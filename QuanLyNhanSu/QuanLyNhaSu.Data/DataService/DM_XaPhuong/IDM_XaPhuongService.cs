﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_XaPhuongService
    {
        Task<int> Create(DM_XaPhuongViewModel request);

        Task<int> Update(DM_XaPhuong request);

        Task<int> Delete(int Id);

        Task<DM_XaPhuong> GetById(int Id);
        IEnumerable<DM_XaPhuong> GetListXaPhuongByQuanHuyen(string MaQuanHuyen);
    }
}