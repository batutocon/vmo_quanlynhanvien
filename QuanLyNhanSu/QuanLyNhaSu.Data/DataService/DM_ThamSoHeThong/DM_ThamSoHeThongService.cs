﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.ViewModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_ThamSoHeThongService : IDM_ThamSoHeThongService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_ThamSoHeThongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_ThamSoHeThongViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_ThamSoHeThong()
            {
                MaThamSo = request.MaThamSo,
                TenThamSo = request.TenThamSo,
                MoTa = request.MoTa,
                TrangThai = request.TrangThai,
                GiaTri = request.GiaTri,
                NgayTao = request.NgayTao,
                NgayCapNhat = request.NgayCapNhat,
                NguoiTao = request.NguoiTao,
                NguoiCapNhat = request.NguoiCapNhat,

            };
            _context.DM_ThamSoHeThongs.Add(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_ThamSoHeThong request)
        {
            var obj = await _context.DM_ThamSoHeThongs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục tham số hệ thống không tồn tại {request.Id}");
            obj.MaThamSo = request.MaThamSo;
            obj.TenThamSo = request.TenThamSo;
            obj.MoTa = request.MoTa;
            obj.TrangThai = request.TrangThai;
            obj.GiaTri = request.GiaTri;
            obj.NgayTao = request.NgayTao;
            obj.NgayCapNhat = request.NgayCapNhat;
            obj.NguoiTao = request.NguoiTao;
            obj.NguoiCapNhat = request.NguoiCapNhat;
            _context.DM_ThamSoHeThongs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_ThamSoHeThongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_ThamSoHeThongs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_ThamSoHeThong> GetById(int Id)
        {
            var objdb = await _context.DM_ThamSoHeThongs.FindAsync(Id);
            if (objdb != null)
            {       
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DM_ThamSoHeThong> GetAllRecords()
        {
            return _context.Set<DM_ThamSoHeThong>().AsNoTracking().ToList();
        }
    }
}