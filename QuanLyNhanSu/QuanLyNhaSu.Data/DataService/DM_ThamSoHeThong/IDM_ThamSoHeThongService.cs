﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_ThamSoHeThongService
    {
        Task<int> Create(DM_ThamSoHeThongViewModel request);

        Task<int> Update(DM_ThamSoHeThong request);

        Task<int> Delete(int Id);

        Task<DM_ThamSoHeThong> GetById(int Id);
        IEnumerable<DM_ThamSoHeThong> GetAllRecords();
    }
}