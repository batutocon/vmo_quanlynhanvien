﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IKhenThuongService
    {
        Task<int> Create(KhenThuongViewModel request);

        //Task<int> Update(PhongBan request);

        Task<int> Delete(int Id);

        Task<KhenThuong> GetById(int Id);

        IEnumerable<KhenThuongViewDetailModel> GetListKhenThuong(int Thang, int Nam);
        KhenThuongViewDetailModel GetDetailKhenThuongByNhanVien(int IdNhanVien,int Thang, int Nam);
    }
}