﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace QuanLyNhanSu.Data.DataService
{
    public class KhenThuongService : IKhenThuongService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public KhenThuongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(KhenThuongViewModel request)
        {
            var obj = new KhenThuong()
            {
                IdNhanVien = request.IdNhanVien,
                Nam = request.Nam,
                Thang = request.Thang,
                NgayTao = DateTime.Now,
                NguoiTao = request.NguoiTao,
                NoiDungKhenThuong = request.NoiDungKhenThuong,
                SoTien = request.SoTien,
            };
            _context.KhenThuongs.Add(obj);
            await _context.SaveChangesAsync();
            return obj.Id;
        }
        //public async Task<int> Update(PhongBan request)
        //{
        //    var obj = await _context.PhongBans.FindAsync(request.Id);

        //    if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Phòng ban không tồn tại {request.Id}");
        //    obj.MaPhongBan = request.MaPhongBan;
        //    obj.TenPhongBan = request.TenPhongBan;
        //    obj.ViTri = request.ViTri;
        //    obj.MoTa = request.MoTa;
        //    _context.PhongBans.Update(obj);
        //    return await _context.SaveChangesAsync();
        //}

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.KhenThuongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Không tồn tại {Id}");
            _context.KhenThuongs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<KhenThuong> GetById(int Id)
        {
            var objdb = await _context.KhenThuongs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<KhenThuongViewDetailModel> GetListKhenThuong(int Thang, int Nam)
        {
            List<KhenThuongViewDetailModel> lst = new List<KhenThuongViewDetailModel>();
            List<NhanVien> lstNV = _context.NhanViens.Where(x=>x.TrangThai == 1).AsNoTracking().ToList();
            foreach(var item in lstNV)
            {
                KhenThuongViewDetailModel obj = new KhenThuongViewDetailModel();
                obj.nhanVien = item;
                var lstKhenthuong  = _context.Set<KhenThuong>().Where(x => x.IdNhanVien == item.Id && x.Thang == Thang && x.Nam == Nam).AsNoTracking().ToList();
                obj.khenThuongs = lstKhenthuong;
                lst.Add(obj);
            }
            return lst;
        }
        public KhenThuongViewDetailModel GetDetailKhenThuongByNhanVien(int IdNhanVien, int Thang, int Nam)
        {
            KhenThuongViewDetailModel obj = new KhenThuongViewDetailModel();
            NhanVien item = _context.Set<NhanVien>().Where(x => x.Id == IdNhanVien).FirstOrDefault();
            obj.nhanVien = item;
            var lstKhenthuong = _context.Set<KhenThuong>().Where(x => x.IdNhanVien == item.Id && x.Thang == Thang && x.Nam == Nam).AsNoTracking().ToList();
            obj.khenThuongs = lstKhenthuong;
            return obj;
        }
    }
}