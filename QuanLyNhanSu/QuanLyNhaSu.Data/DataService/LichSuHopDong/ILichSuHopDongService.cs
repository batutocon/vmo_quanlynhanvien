﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface ILichSuHopDongService
    {
        Task<int> Create(LichSuHopDongViewModel request);

        Task<int> Update(LichSuHopDong request);

        Task<int> Delete(int Id);

        Task<LichSuHopDong> GetById(int Id);

        IEnumerable<NhanVien_HopDongViewModel> GetAllRecords();

        NhanVien_HopDongViewModel NhanVienHopDong(int id);
    }
}