﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace QuanLyNhanSu.Data.DataService
{
    public class LichSuHopDongService : ILichSuHopDongService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public LichSuHopDongService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(LichSuHopDongViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.LichSuHopDong()
            {
                MaHopDong = request.MaHopDong,
                IdLoaiHopDong = request.IdLoaiHopDong,
                IdNhanVien = request.IdNhanVien,
                NgayBatDau = request.NgayBatDau,
                NgayKetThuc = request.NgayKetThuc,
                NgayKy = request.NgayKy,
                NguoiKy = request.NguoiKy,
                TrangThai = request.TrangThai,
                NgayTao = DateTime.Now,
                NguoiTao = request.NguoiTao,
                LuongCB = request.LuongCB,
                IdHeSoLuong = request.IdHeSoLuong
            };
            try
            {
                _context.LichSuHopDongs.Add(obj);
                _context.SaveChanges();
                if (obj.Id < 10)
                {
                    obj.MaHopDong = "HDLD00000" + obj.Id;
                }
                else if (obj.Id > 9 && obj.Id < 100)
                {
                    obj.MaHopDong = "HDLD0000" + obj.Id;
                }
                else if (obj.Id > 99 && obj.Id < 1000)
                {
                    obj.MaHopDong = "HDLD000" + obj.Id;
                }
                else if (obj.Id > 999 && obj.Id < 10000)
                {
                    obj.MaHopDong = "HDLD00" + obj.Id;
                }
                else if (obj.Id > 9999 && obj.Id < 100000)
                {
                    obj.MaHopDong = "HDLD0" + obj.Id;
                }
                else
                {
                    obj.MaHopDong = obj.Id.ToString();
                }
                _context.LichSuHopDongs.Update(obj);
                _context.SaveChanges();

                var nv = _context.NhanViens.FirstOrDefault(x => x.Id == obj.IdNhanVien);
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Thêm mới hợp đồng";
                log.Old = "~";
                log.New = obj.MaHopDong;
                _context.LogNhanViens.Add(log);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            return obj.Id;
        }
        public async Task<int> Update(LichSuHopDong request)
        {
            var obj = await _context.LichSuHopDongs.FindAsync(request.Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Hợp đồng không tồn tại {request.Id}");
            var nv = _context.NhanViens.FirstOrDefault(x => x.Id == obj.IdNhanVien);

            // check người ký
            if (!request.NguoiKy.Equals(obj.NguoiKy))
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin người ký " + obj.MaHopDong;
                log.Old = obj.NguoiKy;
                log.New = request.NguoiKy;
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }
            // check loại hợp đồng
            if (request.IdLoaiHopDong != obj.IdLoaiHopDong)
            {
                var oldObj = _context.DM_LoaiHopDongs.FirstOrDefault(x => x.Id == obj.IdLoaiHopDong);
                var newObj = _context.DM_LoaiHopDongs.FirstOrDefault(x => x.Id == request.IdLoaiHopDong);
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi loại hợp đồng " + obj.MaHopDong;
                log.Old = oldObj != null ? (oldObj.TenLoaiHopDong + "(" + oldObj.MaLoaiHopDong + ")") : "N/A"; ;
                log.New = newObj != null ? (newObj.TenLoaiHopDong + "(" + newObj.MaLoaiHopDong + ")") : "N/A"; ;
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }
            // check ngày bắt đầu
            if (DateTime.Compare(obj.NgayBatDau, request.NgayBatDau) != 0)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin ngày bắt đầu " + obj.MaHopDong;
                log.Old = obj.NgayBatDau.ToString("MM/dd/yyyy");
                log.New = request.NgayBatDau.ToString("MM/dd/yyyy");
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }
            // check ngày kết thúc
            if (DateTime.Compare(obj.NgayKetThuc, request.NgayKetThuc) != 0)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin ngày kết thúc " + obj.MaHopDong;
                log.Old = obj.NgayKetThuc.ToString("MM/dd/yyyy");
                log.New = request.NgayKetThuc.ToString("MM/dd/yyyy");
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }
            // check ngày ký
            if (DateTime.Compare(obj.NgayKy, request.NgayKy) != 0)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin ngày ký " + obj.MaHopDong;
                log.Old = obj.NgayKy.ToString("MM/dd/yyyy");
                log.New = request.NgayKy.ToString("MM/dd/yyyy");
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }

            // check hệ số lương
            if (request.IdHeSoLuong != obj.IdHeSoLuong)
            {
                var oldObj = _context.HeSoLuongs.FirstOrDefault(x => x.Id == obj.IdHeSoLuong);
                var newObj = _context.HeSoLuongs.FirstOrDefault(x => x.Id == request.IdHeSoLuong);
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi hệ số lương " + obj.MaHopDong;
                log.Old = oldObj != null ? (oldObj.HeSo + "(" + oldObj.Ten + ")") : "N/A"; ;
                log.New = newObj != null ? (newObj.HeSo + "(" + newObj.Ten + ")") : "N/A"; ;
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }
            // check lương cb
            if (request.LuongCB != obj.LuongCB)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi thông tin Lương cơ bản " + obj.MaHopDong;
                log.Old = obj.LuongCB.ToString("#.## VND");
                log.New = request.LuongCB.ToString("#.## VND");
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }

            if (request.TrangThai != obj.TrangThai)
            {
                LogNhanVien log = new LogNhanVien();
                log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
                log.TenNhanVien = nv.Ten;
                log.MaNhanVien = nv.IdCBNV;
                log.TaoLuc = DateTime.Now;
                log.TaoBoi = request.NguoiTao;
                log.HanhDong = "Đổi trạng thái hợp đồng " +obj.MaHopDong;
                log.Old = obj.TrangThai == SystemConstants.Web.TRANGTHAI_HOPDONG_COHIEULUC ? "Có hiệu lực" :( obj.TrangThai == SystemConstants.Web.TRANGTHAI_HOPDONG_HETHIEULUC? "Hết hiệu lực" : "Hết hạn hợp đồng");
                log.New = request.TrangThai == SystemConstants.Web.TRANGTHAI_HOPDONG_COHIEULUC ? "Có hiệu lực" : (request.TrangThai == SystemConstants.Web.TRANGTHAI_HOPDONG_HETHIEULUC ? "Hết hiệu lực" : "Hết hạn hợp đồng");
                _context.LogNhanViens.Add(log);_context.SaveChanges();
            }

            obj.IdLoaiHopDong = request.IdLoaiHopDong;
            obj.IdNhanVien = request.IdNhanVien;
            obj.NgayBatDau = request.NgayBatDau;
            obj.NgayKetThuc = request.NgayKetThuc;
            obj.NgayKy = request.NgayKy;
            obj.NguoiKy = request.NguoiKy;
            obj.TrangThai = request.TrangThai;
            obj.NguoiTao = request.NguoiTao;
            obj.LuongCB = request.LuongCB;
            obj.IdHeSoLuong = request.IdHeSoLuong;
            _context.LichSuHopDongs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.LichSuHopDongs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Hợp đồng không tồn tại {Id}");
            _context.LichSuHopDongs.Remove(obj);

            var nv = _context.NhanViens.FirstOrDefault(x => x.Id == obj.IdNhanVien);
            LogNhanVien log = new LogNhanVien();
            log.Type = SystemConstants.Web.LOAILOG_HOPDONG;
            log.TenNhanVien = nv.Ten;
            log.MaNhanVien = nv.IdCBNV;
            log.TaoLuc = DateTime.Now;
            log.TaoBoi = "batutocon";
            log.HanhDong = "Xóa hợp đồng";
            log.Old = "~";
            log.New = obj.MaHopDong;
            _context.LogNhanViens.Add(log);
            _context.SaveChanges();

            return await _context.SaveChangesAsync();
        }

        public async Task<LichSuHopDong> GetById(int Id)
        {
            var objdb = await _context.LichSuHopDongs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<NhanVien_HopDongViewModel> GetAllRecords()
        {
            //   return _context.Set<LichSuHopDong>().AsNoTracking().ToList();
            var lstrs = new List<NhanVien_HopDongViewModel>();
            var lstnv = _context.Set<NhanVien>().Where(x => x.TrangThai == SystemConstants.Web.TRANGTHAI_NHANVIEN_DANGLAMVIEC).AsNoTracking().ToList();
            foreach (var itemNV in lstnv)
            {
                // lấy các hợp đồng vẫn còn hiệu lực
                var hd = _context.LichSuHopDongs.FirstOrDefault(x => x.IdNhanVien == itemNV.Id && x.TrangThai == 1);
                var loaihd = new DM_LoaiHopDong();
                var obj = new NhanVien_HopDongViewModel();
                if (hd == null)
                {
                    hd = new QuanLyNhanSu.Model.Entities.LichSuHopDong()
                    {
                        MaHopDong = "",
                        IdLoaiHopDong = 0,
                        IdNhanVien = 0,
                        NgayBatDau = System.DateTime.Now,
                        NgayKetThuc = System.DateTime.Now,
                        NgayKy = System.DateTime.Now,
                        NguoiKy = ""
                    };
                    obj.TenLoaiHD = "";
                    obj.Id = 0;
                }
                else
                {
                    obj.Id = hd.Id;
                    loaihd = _context.DM_LoaiHopDongs.Find(hd.IdLoaiHopDong);
                    obj.TenLoaiHD = loaihd.TenLoaiHopDong + "(" + loaihd.MaLoaiHopDong + ")";
                }
                obj.MaHopDong = hd.MaHopDong;
                obj.IdLoaiHopDong = hd.IdLoaiHopDong;
                obj.IdNhanVien = itemNV.Id;
                obj.NgayBatDau = hd.NgayBatDau;
                obj.NgayKetThuc = hd.NgayKetThuc;
                obj.NgayKy = hd.NgayKy;
                obj.NguoiKy = hd.NguoiKy;
                obj.TenNV = itemNV.Ten;
                obj.Email = itemNV.Email;
                lstrs.Add(obj);
            }
            return lstrs;
        }
        public NhanVien_HopDongViewModel NhanVienHopDong(int id)
        {
            NhanVien_HopDongViewModel obj = new NhanVien_HopDongViewModel();
            var hd = _context.LichSuHopDongs.Find(id);
            if (hd != null)
            {
                var nv = _context.NhanViens.FirstOrDefault(x => x.Id == hd.IdNhanVien);
                var loaihd = _context.DM_LoaiHopDongs.FirstOrDefault(x => x.Id == hd.IdLoaiHopDong);

                obj.Id = hd.Id;
                obj.TenLoaiHD = loaihd.TenLoaiHopDong + "(" + loaihd.MaLoaiHopDong + ")";
                obj.MaHopDong = hd.MaHopDong;
                obj.IdLoaiHopDong = hd.IdLoaiHopDong;
                obj.IdNhanVien = hd.IdNhanVien;
                obj.NgayBatDau = hd.NgayBatDau;
                obj.NgayKetThuc = hd.NgayKetThuc;
                obj.LuongCB = hd.LuongCB;
                obj.IdHeSoLuong = hd.IdHeSoLuong;
                obj.NgayKy = hd.NgayKy;
                obj.NguoiKy = hd.NguoiKy;
                obj.TenNV = nv.Ten;
                obj.Email = nv.Email;
                obj.TrangThai = hd.TrangThai;
                return obj;
            }
            else
            {
                return null;
            }

        }
    }
}