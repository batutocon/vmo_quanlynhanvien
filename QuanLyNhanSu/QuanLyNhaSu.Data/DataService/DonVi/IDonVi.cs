﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDonViService
    {
        Task<int> Create(DonViViewModel request);

        Task<int> Update(DonVi request);

        Task<int> Delete(int Id);

        Task<DonVi> GetById(int Id);
        IEnumerable<DonVi> GetAllRecords();
    }
}