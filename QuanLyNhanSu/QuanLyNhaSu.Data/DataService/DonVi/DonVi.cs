﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DonViService : IDonViService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DonViService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DonViViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DonVi()
            {
                MaDonVi = request.MaDonVi,
                TenDonVi = request.TenDonVi,
                MoTa = request.MoTa,
            };
            _context.DonVis.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaDonVi = "MDV00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaDonVi = "MDV0" + obj.Id;
            }
            else
            {
                obj.MaDonVi = "MDV" + obj.Id;
            }
            _context.DonVis.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DonVi request)
        {
            var obj = await _context.DonVis.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục đơn vị không tồn tại {request.Id}");
            obj.MaDonVi = request.MaDonVi;
            obj.TenDonVi = request.TenDonVi;
            obj.MoTa = request.MoTa;
            _context.DonVis.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DonVis.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục đơn vị không tồn tại {Id}");
            _context.DonVis.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DonVi> GetById(int Id)
        {
            var objdb = await _context.DonVis.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DonVi> GetAllRecords()
        {
            return _context.Set<DonVi>().AsNoTracking().ToList();
        }


    }
}