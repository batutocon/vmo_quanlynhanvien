﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.ViewModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_QuanhuyenService : IDM_QuanHuyenService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_QuanhuyenService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_QuanHuyenViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_QuanHuyen()
            {
                MaQuanHuyen = request.MaQuanHuyen,
                TenQuanHuyen = request.TenQuanHuyen,
                MaTinhThanh = request.MaTinhThanh,
                TrangThai = request.TrangThai,
            };
            _context.DM_QuanHuyens.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaQuanHuyen = "MQH00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaQuanHuyen = "MQH0" + obj.Id;
            }
            else
            {
                obj.MaQuanHuyen = "MQH" + obj.Id;
            }
            _context.DM_QuanHuyens.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_QuanHuyen request)
        {
            var obj = await _context.DM_QuanHuyens.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục tham số hệ thống không tồn tại {request.Id}");
            obj.MaQuanHuyen = request.MaQuanHuyen;
            obj.TenQuanHuyen = request.TenQuanHuyen;
            obj.MaTinhThanh = request.MaTinhThanh;
            obj.TrangThai = request.TrangThai;
            _context.DM_QuanHuyens.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_QuanHuyens.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục bệnh viện không tồn tại {Id}");
            _context.DM_QuanHuyens.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_QuanHuyen> GetById(int Id)
        {
            var objdb = await _context.DM_QuanHuyens.FindAsync(Id);
            if (objdb != null)
            {       
                return objdb;
            }
            else
            {
                return null;
            }

        }
        public IEnumerable<DM_QuanHuyen> GetListQuanHuyenByTinhThanh(string MaTinhThanh)
        {
            return _context.Set<DM_QuanHuyen>().Where(x => x.MaTinhThanh == MaTinhThanh).ToList();
        }
    }
}