﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_QuanHuyenService
    {
        Task<int> Create(DM_QuanHuyenViewModel request);

        Task<int> Update(DM_QuanHuyen request);

        Task<int> Delete(int Id);

        Task<DM_QuanHuyen> GetById(int Id);

        IEnumerable<DM_QuanHuyen> GetListQuanHuyenByTinhThanh(string MaTinhThanh);

    }
}