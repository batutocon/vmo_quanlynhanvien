﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface INhanVien_TSCPService
    {
        Task<int> Create(NhanVien_TSCPViewModel request);

        Task<int> Update(NhanVien_TSCPUpdateViewModel request);

        Task<int> Delete(int Id);

        Task<NhanVien_TSCP> GetById(int Id);

        IEnumerable<NhanVien_TSCP> GetAllRecords();

        Task<NhanVien_TSCP_DetailViewModel> GetDetailNhanVien_TSCP(int IdNhanVien);

        IEnumerable<NhanVien_TSCP_DetailViewModel> GetAllDetailNhanVien_TSCP();
    }
}