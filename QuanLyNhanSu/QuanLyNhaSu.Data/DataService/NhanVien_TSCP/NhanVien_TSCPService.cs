﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace QuanLyNhanSu.Data.DataService
{
    public class NhanVien_TSCPService : INhanVien_TSCPService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public NhanVien_TSCPService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(NhanVien_TSCPViewModel request)
        {
            var obj = new NhanVien_TSCP()
            {
                IdNhanVien = request.IdNhanVien,
                IdTSCP = request.IdTSCP,
                NgayTao = DateTime.Now,
                NguoiTao = request.NguoiTao,
                NoiDung = request.NoiDung,
                TrangThai = request.TrangThai,
            };
            _context.NhanVien_TSCPs.Add(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(NhanVien_TSCPUpdateViewModel request)
        {
            var obj = await _context.NhanVien_TSCPs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Nhân viên không tồn tại {request.Id}");

            obj.IdTSCP = request.IdTSCP;
            obj.NguoiCapNhat = request.NguoiCapNhat;
            obj.NgayCapNhat = DateTime.Now;
            obj.NoiDung = request.NoiDung;
            obj.TrangThai = request.TrangThai;

            _context.NhanVien_TSCPs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.NhanVien_TSCPs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Nhân viên không tồn tại {Id}");
            _context.NhanVien_TSCPs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<NhanVien_TSCP> GetById(int Id)
        {
            var objdb = await _context.NhanVien_TSCPs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<NhanVien_TSCP> GetAllRecords()
        {
            List<NhanVien_TSCP> lstObj = new List<NhanVien_TSCP>();
            lstObj = _context.NhanVien_TSCPs.AsNoTracking().ToList();
            return lstObj;
        }
        public async Task<NhanVien_TSCP_DetailViewModel>  GetDetailNhanVien_TSCP(int IdNhanVien)
        {
            var obj = await _context.Set<NhanVien_TSCP>().Where(x => x.IdNhanVien == IdNhanVien).FirstOrDefaultAsync();
            NhanVien_TSCP_DetailViewModel nv_tscp_detail = new NhanVien_TSCP_DetailViewModel();
            if (obj != null)
            {
                var nhanvien = await _context.Set<NhanVien>().Where(x => x.Id == IdNhanVien).FirstOrDefaultAsync();
                nv_tscp_detail.nhanVien_TSCP = obj;
                nv_tscp_detail.nhanVien = nhanvien;
                nv_tscp_detail.dM_TaiSanCapPhats = new List<DM_TaiSanCapPhat>();
                string[] listTSCP = obj.IdTSCP.Split(";");
                foreach(string item in listTSCP)
                {
                    string[] child_item = item.Split(":");
                    var tscp = await _context.Set<DM_TaiSanCapPhat>().Where(x => x.MaTaiSanCapPhat == child_item[0]).FirstOrDefaultAsync();
                    if(tscp != null)
                    {
                        int soluong = 1 ;
                        int.TryParse(child_item[1], out soluong);
                        tscp.SoLuong = soluong;
                        nv_tscp_detail.dM_TaiSanCapPhats.Add(tscp);
                    }
                }
            }           
            return nv_tscp_detail;
        }

        public IEnumerable<NhanVien_TSCP_DetailViewModel> GetAllDetailNhanVien_TSCP()
        {
            List<NhanVien> lstNV = _context.NhanViens.AsNoTracking().ToList();
            List<NhanVien_TSCP_DetailViewModel> lstObj = new List<NhanVien_TSCP_DetailViewModel>();
            foreach(NhanVien itemNV in lstNV)
            {
                NhanVien_TSCP_DetailViewModel obj = new NhanVien_TSCP_DetailViewModel();
                obj.nhanVien = itemNV;
                var nv_tscp = _context.Set<NhanVien_TSCP>().Where(x => x.IdNhanVien == itemNV.Id).FirstOrDefault();
                if (nv_tscp != null)
                {
                    obj.nhanVien_TSCP = nv_tscp;
                    obj.dM_TaiSanCapPhats = new List<DM_TaiSanCapPhat>();
                    string[] listTSCP = nv_tscp.IdTSCP.Split(";");
                    foreach (string itemStr in listTSCP)
                    {
                        string[] child_item = itemStr.Split(":");
                        var tscp = _context.Set<DM_TaiSanCapPhat>().Where(x => x.MaTaiSanCapPhat == child_item[0]).FirstOrDefault();
                        if (tscp != null)
                        {
                            int soluong = 1;
                            int.TryParse(child_item[1], out soluong);
                            tscp.SoLuong = soluong;
                            obj.dM_TaiSanCapPhats.Add(tscp);
                        }
                    }
                }
                else
                {
                    obj.nhanVien_TSCP = new NhanVien_TSCP();
                    obj.dM_TaiSanCapPhats = new List<DM_TaiSanCapPhat>();
                }
                lstObj.Add(obj);
            }
            return lstObj;
        }
    }
}