﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuanLyNhanSu.Model.Entities;

namespace QuanLyNhanSu.Data.DataService
{
    public interface IDM_ChiNhanhNganHangService
    {
        Task<int> Create(DM_ChiNhanhNganHangViewModel request);

        Task<int> Update(DM_ChiNhanhNganHang request);

        Task<int> Delete(int Id);

        Task<DM_ChiNhanhNganHang> GetById(int Id);

        IEnumerable<DM_ChiNhanhNganHang> GetAllRecords();
    }
}