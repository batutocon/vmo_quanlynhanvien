﻿using QuanLyNhanSu.Model;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace QuanLyNhanSu.Data.DataService
{
    public class DM_ChiNhanhNganHangService : IDM_ChiNhanhNganHangService
    {
        private readonly QuanLyNhanSuDbContext _context;

        public DM_ChiNhanhNganHangService(QuanLyNhanSuDbContext context)
        {
            _context = context;
        }

        public async Task<int> Create(DM_ChiNhanhNganHangViewModel request)
        {
            var obj = new QuanLyNhanSu.Model.Entities.DM_ChiNhanhNganHang()
            {
                MaChiNhanh = request.MaChiNhanh,
                TenChiNhanh = request.TenChiNhanh,
                TrangThai = request.TrangThai,
                GhiChu = request.GhiChu,
                TenNganHang = request.TenNganHang,
                MaNganHang = request.MaNganHang,
            };
            _context.DM_ChiNhanhNganHangs.Add(obj);
            _context.SaveChanges();
            if (obj.Id < 10)
            {
                obj.MaChiNhanh = "MCN00" + obj.Id;
            }
            else if (obj.Id > 9 && obj.Id < 100)
            {
                obj.MaChiNhanh = "MCN0" + obj.Id;
            }
            else
            {
                obj.MaChiNhanh = "MCN" + obj.Id;
            }
            _context.DM_ChiNhanhNganHangs.Update(obj);
            _context.SaveChanges();
            return obj.Id;
        }
        public async Task<int> Update(DM_ChiNhanhNganHang request)
        {
            var obj = await _context.DM_ChiNhanhNganHangs.FindAsync(request.Id);

            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục chi nhánh không tồn tại {request.Id}");
            obj.MaChiNhanh = request.MaChiNhanh;
            obj.TenChiNhanh = request.TenChiNhanh;
            obj.MaNganHang = request.MaNganHang;
            obj.TenNganHang = request.TenNganHang;
            obj.TrangThai = request.TrangThai;
            obj.GhiChu = request.GhiChu;
            _context.DM_ChiNhanhNganHangs.Update(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> Delete(int Id)
        {
            var obj = await _context.DM_ChiNhanhNganHangs.FindAsync(Id);
            if (obj == null) throw new QuanLyNhanSu.Common.Exceptions.QuanLyNhanSu($"Danh mục chi nhánh không tồn tại {Id}");
            _context.DM_ChiNhanhNganHangs.Remove(obj);
            return await _context.SaveChangesAsync();
        }

        public async Task<DM_ChiNhanhNganHang> GetById(int Id)
        {
            var objdb = await _context.DM_ChiNhanhNganHangs.FindAsync(Id);
            if (objdb != null)
            {
                return objdb;
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DM_ChiNhanhNganHang> GetAllRecords()
        {
            return _context.Set<DM_ChiNhanhNganHang>().AsNoTracking().ToList();
        }
    }
}