﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_QuanHuyenConfiguration : IEntityTypeConfiguration<DM_QuanHuyen>
    {
        public void Configure(EntityTypeBuilder<DM_QuanHuyen> builder)
        {
            builder.ToTable("DM_QuanHuyen");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
