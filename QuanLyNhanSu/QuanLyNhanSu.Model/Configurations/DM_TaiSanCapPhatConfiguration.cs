﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Configurations
{
    class DM_TaiSanCapPhatConfiguration : IEntityTypeConfiguration<DM_TaiSanCapPhat>
    {
        public void Configure(EntityTypeBuilder<DM_TaiSanCapPhat> builder)
        {
            builder.ToTable("DM_TaiSanCapPhat");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
