﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class QLQuyetDinhConfiguration : IEntityTypeConfiguration<QLQuyetDinh>
    {
        public void Configure(EntityTypeBuilder<QLQuyetDinh> builder)
        {
            builder.ToTable("QLQuyetDinh");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
