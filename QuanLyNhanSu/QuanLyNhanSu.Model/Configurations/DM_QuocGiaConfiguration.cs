﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_QuocGiaConfiguration : IEntityTypeConfiguration<DM_QuocGia>
    {
        public void Configure(EntityTypeBuilder<DM_QuocGia> builder)
        {
            builder.ToTable("DM_QuocGia");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
