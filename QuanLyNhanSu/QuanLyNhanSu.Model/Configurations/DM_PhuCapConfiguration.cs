﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_PhuCapConfiguration : IEntityTypeConfiguration<DM_PhuCap>
    {
        public void Configure(EntityTypeBuilder<DM_PhuCap> builder)
        {
            builder.ToTable("DM_PhuCap");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
