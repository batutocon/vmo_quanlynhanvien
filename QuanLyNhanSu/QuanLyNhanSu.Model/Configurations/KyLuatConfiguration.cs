﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class KyLuatConfiguration : IEntityTypeConfiguration<KyLuat>
    {
        public void Configure(EntityTypeBuilder<KyLuat> builder)
        {
            builder.ToTable("KyLuat");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
