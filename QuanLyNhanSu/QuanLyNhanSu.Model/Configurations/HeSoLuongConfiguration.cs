﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class HeSoLuongConfiguration : IEntityTypeConfiguration<HeSoLuong>
    {
        public void Configure(EntityTypeBuilder<HeSoLuong> builder)
        {
            builder.ToTable("HeSoLuong");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
