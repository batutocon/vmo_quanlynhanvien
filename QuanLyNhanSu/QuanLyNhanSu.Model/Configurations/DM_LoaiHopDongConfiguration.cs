﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_LoaiHopDongConfiguration : IEntityTypeConfiguration<DM_LoaiHopDong>
    {
        public void Configure(EntityTypeBuilder<DM_LoaiHopDong> builder)
        {
            builder.ToTable("DM_LoaiHopDong");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
