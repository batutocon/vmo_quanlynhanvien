﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_ChucDanhConfiguration : IEntityTypeConfiguration<DM_ChucDanh>
    {
        public void Configure(EntityTypeBuilder<DM_ChucDanh> builder)
        {
            builder.ToTable("DM_ChucDanh");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
