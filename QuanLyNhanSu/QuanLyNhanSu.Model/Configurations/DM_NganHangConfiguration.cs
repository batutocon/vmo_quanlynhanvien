﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_NganHangConfiguration : IEntityTypeConfiguration<DM_NganHang>
    {
        public void Configure(EntityTypeBuilder<DM_NganHang> builder)
        {
            builder.ToTable("DM_NganHang");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
