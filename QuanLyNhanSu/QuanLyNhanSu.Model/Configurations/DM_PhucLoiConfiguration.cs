﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_PhucLoiConfiguration : IEntityTypeConfiguration<DM_PhucLoi>
    {
        public void Configure(EntityTypeBuilder<DM_PhucLoi> builder)
        {
            builder.ToTable("DM_PhucLoi");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
