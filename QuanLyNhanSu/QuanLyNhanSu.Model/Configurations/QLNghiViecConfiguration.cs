﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class QLNghiViecConfiguration : IEntityTypeConfiguration<QLNghiViec>
    {
        public void Configure(EntityTypeBuilder<QLNghiViec> builder)
        {
            builder.ToTable("QLNghiViec");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
