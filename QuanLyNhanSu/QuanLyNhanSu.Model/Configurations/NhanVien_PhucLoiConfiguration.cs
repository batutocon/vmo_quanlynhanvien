﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class NhanVien_PhucLoiConfiguration : IEntityTypeConfiguration<NhanVien_PhucLoi>
    {
        public void Configure(EntityTypeBuilder<NhanVien_PhucLoi> builder)
        {
            builder.ToTable("NhanVien_PhucLoi");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
