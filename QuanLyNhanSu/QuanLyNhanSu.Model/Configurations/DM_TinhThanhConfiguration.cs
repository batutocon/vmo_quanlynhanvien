﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_TinhThanhConfiguration : IEntityTypeConfiguration<DM_TinhThanh>
    {
        public void Configure(EntityTypeBuilder<DM_TinhThanh> builder)
        {
            builder.ToTable("DM_TinhThanh");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
