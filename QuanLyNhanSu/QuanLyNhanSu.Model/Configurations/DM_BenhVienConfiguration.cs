﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_BenhVienConfiguration : IEntityTypeConfiguration<DM_BenhVien>
    {
        public void Configure(EntityTypeBuilder<DM_BenhVien> builder)
        {
            builder.ToTable("DM_BenhVien");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
