﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class BaoHiemConfiguration : IEntityTypeConfiguration<BaoHiem>
    {
        public void Configure(EntityTypeBuilder<BaoHiem> builder)
        {
            builder.ToTable("BaoHiem");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
