﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_ChiNhanhNganHangConfiguration : IEntityTypeConfiguration<DM_ChiNhanhNganHang>
    {
        public void Configure(EntityTypeBuilder<DM_ChiNhanhNganHang> builder)
        {
            builder.ToTable("DM_ChiNhanhNganHang");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
