﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class LichSuHopDongConfiguration : IEntityTypeConfiguration<LichSuHopDong>
    {
        public void Configure(EntityTypeBuilder<LichSuHopDong> builder)
        {
            builder.ToTable("LichSuHopDong");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
