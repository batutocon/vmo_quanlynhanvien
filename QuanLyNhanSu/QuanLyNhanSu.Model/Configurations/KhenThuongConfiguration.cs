﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class KhenThuongConfiguration : IEntityTypeConfiguration<KhenThuong>
    {
        public void Configure(EntityTypeBuilder<KhenThuong> builder)
        {
            builder.ToTable("KhenThuong");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
