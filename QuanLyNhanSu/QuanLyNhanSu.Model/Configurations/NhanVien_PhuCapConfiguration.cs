﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class NhanVien_PhuCapConfiguration : IEntityTypeConfiguration<NhanVien_PhuCap>
    {
        public void Configure(EntityTypeBuilder<NhanVien_PhuCap> builder)
        {
            builder.ToTable("NhanVien_PhuCap");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
