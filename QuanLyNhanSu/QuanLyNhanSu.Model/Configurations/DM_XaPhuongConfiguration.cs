﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Configurations
{
    public class DM_XaPhuongConfiguration : IEntityTypeConfiguration<DM_XaPhuong>
    {
        public void Configure(EntityTypeBuilder<DM_XaPhuong> builder)
        {
            builder.ToTable("DM_XaPhuong");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).UseIdentityColumn();
        }
    }
}
