﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_LoaiHopDong
    {
        public int Id { set; get; }
        public string MaLoaiHopDong { set; get; }
        public string TenLoaiHopDong { set; get; }
        public string ThoiHanHopDong { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
    }
}
