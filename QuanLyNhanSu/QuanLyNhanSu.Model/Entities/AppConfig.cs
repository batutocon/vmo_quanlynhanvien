﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class AppConfig
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
