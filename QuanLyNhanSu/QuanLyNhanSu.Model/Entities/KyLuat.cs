﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class KyLuat
    {
        public int Id { get; set; }
        public int IdNhanVien { get; set; }
        public string NoiDungKyLuat { get; set; }
        public double SoTien { get; set; }
        public int Thang { get; set; }
        public int Nam { get; set; }
        public DateTime NgayTao { get; set; }
        public string NguoiTao { get; set; }
    }
}
