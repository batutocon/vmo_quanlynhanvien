﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class LichSuHopDong
    {
        public int Id { get; set; }
        public string MaHopDong { get; set; }
        public int IdLoaiHopDong { get; set; }
        public int IdNhanVien { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public DateTime NgayKy { get; set; }
        public string NguoiKy { get; set; }
        public int TrangThai { get; set; }
        public DateTime NgayTao { get; set; }
        public string NguoiTao { get; set; }
        public double LuongCB { get; set; }
        public int IdHeSoLuong { get; set; }
    }
}
