﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class LogNhanVien
    {
        public int Id { get; set; }
        public string MaNhanVien { get; set; }
        public string TenNhanVien { get; set; }
        public string Old { get; set; }
        public string New { get; set; }
        public string HanhDong { get; set; }
        public DateTime TaoLuc { get; set; }
        public string TaoBoi { get; set; }
        public int Type { get; set; }

    }
}
