﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_BenhVien
    {
        public int Id { set; get; }
        public string MaBenhVien { set; get; }
        public string TenBenhVien { set; get; }
        public string DiaChi { set; get; }
        public string MoTa { set; get; }
       
    }
}
