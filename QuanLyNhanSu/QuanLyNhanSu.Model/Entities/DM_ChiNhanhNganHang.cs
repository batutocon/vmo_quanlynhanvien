﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_ChiNhanhNganHang
    {
        public int Id { set; get; }
        public string MaChiNhanh { set; get; }
        public string TenChiNhanh { set; get; }
        public string MaNganHang { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
        public string TenNganHang { set; get; }

    }
}
