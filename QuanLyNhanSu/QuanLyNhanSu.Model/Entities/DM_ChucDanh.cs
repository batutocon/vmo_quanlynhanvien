﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_ChucDanh
    {
        public int Id { set; get; }
        public string MaChucDanh { set; get; }
        public string TenChucDanh { set; get; }
        public string MoTa { set; get; }

        public string IdDonVi { set; get; }
    }
}
