﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_NganHang
    {
        public int Id { set; get; }
        public string MaNganHang { set; get; }
        public string TenNganHang { set; get; }
        public string TenVietTat { set; get; }
        public int TrangThai { set; get; }

    }
}
