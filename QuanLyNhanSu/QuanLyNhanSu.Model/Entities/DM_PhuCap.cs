﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_PhuCap
    {
        public int Id { set; get; }
        public string MaPhuCap { set; get; }
        public string LoaiThuong { set; get; }
        public string NguonChi { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
        public double SoTienTheoLuongCB { set; get; }
    }
}
