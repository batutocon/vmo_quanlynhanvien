﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class HeSoLuong
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public double HeSo { get; set; }
        public int TrangThai { get; set; }
        public DateTime NgayTao { get; set; }
        public string NguoiTao { get; set; }
    }
}
