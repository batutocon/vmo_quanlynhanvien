﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_TinhThanh
    {
        public int Id { set; get; }
        public string MaQuocGia { set; get; }
        public string MaTinhThanh { set; get; }
        public string TenTinhThanh { set; get; }
        public int TrangThai { set; get; }
    }
}
