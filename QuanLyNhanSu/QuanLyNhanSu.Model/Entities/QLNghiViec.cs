﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class QLNghiViec
    {
        public int Id { get; set; }
        public int IdNhanVien { get; set; }
        public string Note { get; set; }
        public int TrangThai { get; set; }
        public string TaoBoi { get; set; }
        public DateTime TaoLuc { get; set; }     
    }
}
