﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_ThamSoHeThong
    {
        public int Id { set; get; }
        public string MaThamSo { set; get; }
        public string TenThamSo { set; get; }
        public string MoTa { set; get; }
        public int TrangThai { set; get; }
        public string GiaTri { set; get; }
        public DateTime NgayTao { set; get; }
        public DateTime NgayCapNhat { set; get; }
        public string NguoiTao { set; get; }
        public string NguoiCapNhat { set; get; }
    }
}
