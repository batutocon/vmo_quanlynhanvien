﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_PhucLoi
    {
        public int Id { set; get; }
        public string MaCheDoPhucLoi { set; get; }
        public string TenPhucLoi { set; get; }
        public string SoTienTruocThue { set; get; }
        public DateTime NgayHieuLuc { set; get; }
        public DateTime NgayHetHieuLuc { set; get; }
        public string NguonChi { set; get; }
        public int TrangThai { set; get; }
        public string MucChiToiDa { set; get; }

    }
}
