﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_QuocGia
    {
        public int Id { set; get; }
        public string MaQuocGia { set; get; }
        public string TenQuocGia { set; get; }
        public int TrangThai { set; get; }

    }
}
