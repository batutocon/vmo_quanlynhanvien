﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class BaoHiem
    {
        public int Id { set; get; }
        public DateTime NgayBatDau { set; get; }
        public DateTime ThoiHanBaoHiemDen { set; get; }
      
        public int IdDonVi { set; get; }
        

    }
}
