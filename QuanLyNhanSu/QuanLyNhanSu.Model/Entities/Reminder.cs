﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class Reminder
    {
        public int Id { get; set; }
        public int IdNhanVien { get; set; }
        public int IdLoaiHopDong { get; set; }
        public string MoTa { get; set; }
    }
}
