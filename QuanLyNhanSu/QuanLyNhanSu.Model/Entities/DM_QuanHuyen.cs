﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_QuanHuyen
    {
        public int Id { set; get; }
        public string MaTinhThanh { set; get; }
        public string MaQuanHuyen { set; get; }
        public string TenQuanHuyen { set; get; }
        public int TrangThai { set; get; }
    }
}
