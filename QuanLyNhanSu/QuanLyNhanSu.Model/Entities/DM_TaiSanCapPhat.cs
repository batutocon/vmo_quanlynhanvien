﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_TaiSanCapPhat
    {
        public int Id { set; get; }
        public string MaTaiSanCapPhat { set; get; }  
        public string TenTaiSanCapPhat { get; set; }
        public int SoLuong { set; get; }
        public string MoTa { get; set; }
    }
}
