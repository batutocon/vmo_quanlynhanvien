﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class QLQuyetDinh
    {
        public int Id { get; set; }
        public int IdNhanVien { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string NguoiTao { get; set; }
        public DateTime NgayTao { get; set; }
        public string NguoiCapNhat { get; set; }
        public DateTime NgayCapNhat { get; set; }       
    }
}
