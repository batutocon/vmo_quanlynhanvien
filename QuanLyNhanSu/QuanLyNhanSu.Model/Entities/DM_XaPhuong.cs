﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyNhanSu.Model.Entities
{
    public class DM_XaPhuong
    {
        public int Id { set; get; }
        public string MaQuanHuyen { set; get; }
        public string MaXaPhuong { set; get; }
        public string TenXaPhuong { set; get; }
        public int TrangThai { set; get; }
        public string GhiChu { set; get; }
    }
}
