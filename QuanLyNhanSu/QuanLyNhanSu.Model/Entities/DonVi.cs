﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class DonVi
    {
        public int Id { get; set; }
        public string MaDonVi { get; set; }
        public string TenDonVi { get; set; }
        public string MoTa { get; set; }
    }
}
