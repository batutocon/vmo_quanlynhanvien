﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Model.Entities
{
    public class NhanVien
    {
        public int Id { get; set; }
        public string IdCBNV { get; set; }
        public string Ten { get; set; }
        public string GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string CanCuoc_CMND { get; set; }
        public DateTime NgayCap { get; set; }
        public string NoiCap { get; set; }
        public string Email { get; set; }
        public int ChucVu { get; set; }
        public int PhongBan { get; set; }
        public int TrangThai { get; set; }
        public DateTime NgayTao { get; set; }
        public string NguoiTao { get; set; }

    }
}
