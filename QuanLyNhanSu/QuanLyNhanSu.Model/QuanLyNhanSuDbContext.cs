﻿using QuanLyNhanSu.Model.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using QuanLyNhanSu.Model.Configurations;

namespace QuanLyNhanSu.Model
{
    public class QuanLyNhanSuDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public QuanLyNhanSuDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure using Fluent API
            modelBuilder.ApplyConfiguration(new AppConfigConfiguration());
            modelBuilder.ApplyConfiguration(new AppUserConfiguration());
            modelBuilder.ApplyConfiguration(new AppRoleConfiguration());
            modelBuilder.ApplyConfiguration(new BaoHiemConfiguration());
            modelBuilder.ApplyConfiguration(new DM_BenhVienConfiguration());
            modelBuilder.ApplyConfiguration(new DM_ChiNhanhNganHangConfiguration());
            modelBuilder.ApplyConfiguration(new DM_ChucDanhConfiguration());
            modelBuilder.ApplyConfiguration(new DM_LoaiHopDongConfiguration());
            modelBuilder.ApplyConfiguration(new DM_ChiNhanhNganHangConfiguration());
            modelBuilder.ApplyConfiguration(new DM_NganHangConfiguration());
            modelBuilder.ApplyConfiguration(new DM_PhuCapConfiguration());
            modelBuilder.ApplyConfiguration(new DM_PhucLoiConfiguration());
            modelBuilder.ApplyConfiguration(new DM_QuanHuyenConfiguration());
            modelBuilder.ApplyConfiguration(new DM_QuocGiaConfiguration());
            modelBuilder.ApplyConfiguration(new DM_TaiSanCapPhatConfiguration());
            modelBuilder.ApplyConfiguration(new DM_ThamSoHeThongConfiguration());
            modelBuilder.ApplyConfiguration(new DM_TinhThanhConfiguration());
            modelBuilder.ApplyConfiguration(new DM_XaPhuongConfiguration());
            modelBuilder.ApplyConfiguration(new DonViConfiguration());
            modelBuilder.ApplyConfiguration(new KhenThuongConfiguration());
            modelBuilder.ApplyConfiguration(new KyLuatConfiguration());
            modelBuilder.ApplyConfiguration(new NhanVienConfiguration());
            modelBuilder.ApplyConfiguration(new LichSuHopDongConfiguration());
            modelBuilder.ApplyConfiguration(new PhongBanConfiguration());
            modelBuilder.ApplyConfiguration(new ReminderConfiguration());
            modelBuilder.ApplyConfiguration(new QLQuyetDinhConfiguration());
            modelBuilder.ApplyConfiguration(new QLNghiViecConfiguration());
            modelBuilder.ApplyConfiguration(new HeSoLuongConfiguration());
            modelBuilder.ApplyConfiguration(new NhanVien_PhuCapConfiguration());
            modelBuilder.ApplyConfiguration(new NhanVien_PhucLoiConfiguration());
            modelBuilder.ApplyConfiguration(new NhanVien_TSCPConfiguration());
            modelBuilder.ApplyConfiguration(new BangLuongConfiguration());
            modelBuilder.ApplyConfiguration(new LogNhanVienConfiguration());


            modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims");
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRoles").HasKey(x => new { x.UserId, x.RoleId });
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);
            modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims");
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens").HasKey(x => x.UserId);
            //modelBuilder.Seed();
        }

        public DbSet<AppConfig> AppConfigs { get; set; }
        public DbSet<AppRole> AppRoles { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }     
        public DbSet<BaoHiem> BaoHiems { get; set; }
        public DbSet<DM_BenhVien> DM_BenhViens { get; set; }
        public DbSet<DM_ChiNhanhNganHang> DM_ChiNhanhNganHangs { get; set; }
        public DbSet<DM_ChucDanh> DM_ChucDanhs { get; set; }
        public DbSet<DM_LoaiHopDong> DM_LoaiHopDongs { get; set; }    
        public DbSet<DM_NganHang> DM_NganHangs { get; set; }
        public DbSet<DM_PhuCap> DM_PhuCaps { get; set; }
        public DbSet<DM_PhucLoi> DM_PhucLois { get; set; }
        public DbSet<DM_QuanHuyen> DM_QuanHuyens { get; set; }
        public DbSet<DM_QuocGia> DM_QuocGias { get; set; }
        public DbSet<DM_TaiSanCapPhat> DM_TaiSanCapPhats { get; set; }
        public DbSet<DM_ThamSoHeThong> DM_ThamSoHeThongs { get; set; }
        public DbSet<DM_TinhThanh> DM_TinhThanhs { get; set; }
        public DbSet<DM_XaPhuong> DM_XaPhuongs { get; set; }
        public DbSet<DonVi> DonVis { get; set; }
        public DbSet<KhenThuong> KhenThuongs { get; set; }
        public DbSet<KyLuat> KyLuats { get; set; }
        public DbSet<NhanVien> NhanViens { get; set; }
        public DbSet<LichSuHopDong> LichSuHopDongs { get; set; }
        public DbSet<PhongBan> PhongBans { get; set; }
        public DbSet<Reminder> Reminders { get; set; }
        public DbSet<QLQuyetDinh> QLQuyetDinhs { get; set; }
        public DbSet<QLNghiViec> QLNghiViecs { get; set; }
        public DbSet<HeSoLuong> HeSoLuongs { get; set; }
        public DbSet<NhanVien_PhuCap> NhanVien_PhuCaps { get; set; }
        public DbSet<NhanVien_PhucLoi> NhanVien_PhucLois { get; set; }
        public DbSet<NhanVien_TSCP> NhanVien_TSCPs { get; set; }
        public DbSet<BangLuong> BangLuongs { get; set; }
        public DbSet<LogNhanVien> LogNhanViens { get; set; }
    }
}