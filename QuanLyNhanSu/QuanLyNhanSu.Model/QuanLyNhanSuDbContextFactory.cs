﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace QuanLyNhanSu.Model
{
    public class QuanLyNhanSuDbContextFactory : IDesignTimeDbContextFactory<QuanLyNhanSuDbContext>
    {
        public QuanLyNhanSuDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("QuanLyNhanSuSolutionDb");

            var optionsBuilder = new DbContextOptionsBuilder<QuanLyNhanSuDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            return new QuanLyNhanSuDbContext(optionsBuilder.Options);
        }
    }
}
