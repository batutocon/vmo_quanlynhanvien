﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Service.Models
{
    public class BangLuongCreateRequest
    {
        public int IdNhanVien { get; set; }
        public int Thang { get; set; }
        public int Nam { get; set; }
        public int IdLichSuHopDong { get; set; }
        public string NguoiTao { get; set; }
        public DateTime NgayTao { get; set; }
        public int TrangThai { get; set; }
    }
    public class BangLuongUpdateRequest
    {
        public int Id { get; set; }
        public double TienPhuCap { get; set; }
        public double TienPhucLoi { get; set; }
        public double TienThuong { get; set; }
        public double TienPhat { get; set; }
        public double TongLuong { get; set; }
        public int TrangThai { get; set; }
        public DateTime NgayNhanLuong { get; set; }
    }
}
