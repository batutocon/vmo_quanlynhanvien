﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/LogNhanViens
    [Route("api/[controller]")]
    [ApiController]
    public class LogNhanVienController : ControllerBase
    {
        private readonly ILogNhanVienService _dbContext;

        public LogNhanVienController(ILogNhanVienService dbContext)
        {
            _dbContext = dbContext;
        }      

        [HttpPost("GetAllRecords")]
        public IActionResult GetAllRecords([FromForm] string TuNgay, [FromForm] string DenNgay, [FromForm] int type)
        {
            IEnumerable<LogNhanVien> lst = _dbContext.GetAllRecords(TuNgay, DenNgay, type);
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }
    }
}