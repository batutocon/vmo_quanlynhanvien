﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel;
using System.Collections.Generic;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/products
    [Route("api/[controller]")]
    [ApiController]
    public class DM_XaPhuongController : ControllerBase
    {
        private readonly IDM_XaPhuongService _dbContext;

        public DM_XaPhuongController(IDM_XaPhuongService dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            var obj = await _dbContext.GetById(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }


        [HttpPost]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Create([FromForm] DM_XaPhuongViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Id = await _dbContext.Create(request);
            if (Id == 0)
                return BadRequest();

            var product = await _dbContext.GetById(Id);

            return CreatedAtAction(nameof(GetById), new { id = Id }, product);
        }

        [HttpPut("{Id}")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Update([FromRoute] int Id, [FromForm] DM_XaPhuongViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DM_XaPhuong obj = new DM_XaPhuong()
            {
                Id = Id,
                MaXaPhuong = request.MaXaPhuong,
                TenXaPhuong = request.TenXaPhuong,
                MaQuanHuyen = request.MaQuanHuyen,
                TrangThai = request.TrangThai,
                GhiChu = request.GhiChu,
            };
            var result = await _dbContext.Update(obj);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpDelete("{Id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            var result = await _dbContext.Delete(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpGet("GetListXaPhuongByQuanHuyen/{maQuanHuyen}")]
        public IActionResult GetListXaPhuongByQuanHuyen(string maQuanHuyen)
        {
            IEnumerable<DM_XaPhuong> lst = _dbContext.GetListXaPhuongByQuanHuyen(maQuanHuyen);
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }
    }
}