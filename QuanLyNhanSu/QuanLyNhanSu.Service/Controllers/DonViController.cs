﻿
using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Model.Entities;
using System.Collections.Generic;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/products
    [Route("api/[controller]")]
    [ApiController]
    public class DonViController : ControllerBase
    {
        private readonly IDonViService _dbContext;

        public DonViController(IDonViService dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            var obj = await _dbContext.GetById(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }


        [HttpPost]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Create([FromForm] DonViViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Id = await _dbContext.Create(request);
            if (Id == 0)
                return BadRequest();

            var product = await _dbContext.GetById(Id);

            return CreatedAtAction(nameof(GetById), new { id = Id }, product);
        }

        [HttpPut("{Id}")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Update([FromRoute] int Id, [FromForm] DonViViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DonVi obj = new DonVi()
            {
                Id = Id,
                MaDonVi = request.MaDonVi,
                TenDonVi = request.TenDonVi,
                MoTa = request.MoTa,
            };
            var result = await _dbContext.Update(obj);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpDelete("{Id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            var result = await _dbContext.Delete(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpGet("GetAllRecords")]
        public IActionResult GetAllRecords()
        {
            IEnumerable<DonVi> lst = _dbContext.GetAllRecords();
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }
    }
}