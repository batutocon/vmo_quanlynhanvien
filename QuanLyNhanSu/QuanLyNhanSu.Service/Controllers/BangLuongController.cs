﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/products
    [Route("api/[controller]")]
    [ApiController]
    public class BangLuongController : ControllerBase
    {
        private readonly IBangLuongService _dbContext;

        public BangLuongController(IBangLuongService dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            var obj = await _dbContext.GetById(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }


        [HttpPost]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Create([FromForm] BangLuongCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            BangLuongViewModel obj = new BangLuongViewModel()
            {
                IdLichSuHopDong = request.IdLichSuHopDong,
                IdNhanVien = request.IdNhanVien,
                Nam = request.Nam,
                Thang = request.Thang,
                NgayTao = DateTime.Now,
                NguoiTao = request.NguoiTao,
                TrangThai = request.TrangThai,
            };
            var Id = await _dbContext.Create(obj);
            if (Id == 0)
                return BadRequest();

            var product = await _dbContext.GetById(Id);

            return CreatedAtAction(nameof(GetById), new { id = Id }, product);
        }

        [HttpPut("TinhLuong")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> UpdateTinhLuong([FromForm] int Id, [FromForm] double luongCB, [FromForm] int heso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _dbContext.UpdateTinhLuong(Id,luongCB,heso);
            if (result == 0)
                return BadRequest();
            return Ok();
        }
        [HttpPut("ThuongPhat")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> UpdateThuongPhat([FromForm] int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _dbContext.UpdateThuongPhat(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpPut("NhanLuong")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> UpdateNhanLuong([FromForm] int Id, [FromForm] int TrangThai)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _dbContext.UpdateNhanLuong(Id,TrangThai);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpDelete("{Id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            var result = await _dbContext.Delete(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpGet("GetListBangLuongTheoThangNam/Thang={Thang}&&Nam={Nam}")]
        public IActionResult GetListBangLuongTheoThangNam([FromRoute] int Thang, [FromRoute] int Nam)
        {
            IEnumerable<BangLuongTheoThangNam> lst = _dbContext.GetListBangLuongTheoThangNam(Thang,Nam);
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }

        [HttpGet("GetAllRecords")]
        public IActionResult GetAllRecords()
        {
            IEnumerable<BangLuong> lst = _dbContext.GetAllRecords();
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }
    }
}
