﻿using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Model.Entities;
using System.Collections.Generic;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/LichSuHopDongs
    [Route("api/[controller]")]
    [ApiController]
    public class LichSuHopDongController : ControllerBase
    {
        private readonly ILichSuHopDongService _dbContext;

        public LichSuHopDongController(ILichSuHopDongService dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            var obj = await _dbContext.GetById(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }


        [HttpPost]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Create([FromForm] LichSuHopDongViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Id = await _dbContext.Create(request);
            if (Id == 0)
                return BadRequest();

            var LichSuHopDong = await _dbContext.GetById(Id);

            return CreatedAtAction(nameof(GetById), new { id = Id }, LichSuHopDong);
        }

        [HttpPut("{Id}")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Update([FromRoute] int Id, [FromForm] LichSuHopDongViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            LichSuHopDong obj = new LichSuHopDong()
            {
                Id = Id,
                MaHopDong = request.MaHopDong,
                IdLoaiHopDong = request.IdLoaiHopDong,
                IdNhanVien = request.IdNhanVien,
                NgayBatDau = request.NgayBatDau,
                NgayKetThuc = request.NgayKetThuc,
                NgayKy = request.NgayKy,
                TrangThai = request.TrangThai,              
                NguoiKy = request.NguoiKy,
                NguoiTao = request.NguoiTao,
                LuongCB = request.LuongCB,
                IdHeSoLuong = request.IdHeSoLuong
            };
            var result = await _dbContext.Update(obj);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpDelete("{Id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            var result = await _dbContext.Delete(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpGet("GetAllRecords")]
        public IActionResult GetAllRecords()
        {
            IEnumerable<NhanVien_HopDongViewModel> lst = _dbContext.GetAllRecords();
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }

        [HttpGet("NhanVienHopDong/{Id}")]
        public IActionResult NhanVienHopDong(int Id)
        {
            NhanVien_HopDongViewModel obj = _dbContext.NhanVienHopDong(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }
        
    }
}