﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/NhanVien_TSCP
    [Route("api/[controller]")]
    [ApiController]
    public class NhanVien_TSCPController : ControllerBase
    {
        private readonly INhanVien_TSCPService _dbContext;

        public NhanVien_TSCPController(INhanVien_TSCPService dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            var obj = await _dbContext.GetById(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }


        [HttpPost("Create")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Create([FromForm] NhanVien_TSCPViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Id = await _dbContext.Create(request);
            if (Id == 0)
                return BadRequest();

            var LichSuHopDong = await _dbContext.GetById(Id);

            return CreatedAtAction(nameof(GetById), new { id = Id }, LichSuHopDong);
        }

        [HttpPut("Update")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Update([FromForm] NhanVien_TSCPUpdateViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _dbContext.Update(request);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpDelete("Delete/{Id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            var result = await _dbContext.Delete(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        [HttpGet("GetAllRecords")]
        public IActionResult GetAllRecords()
        {
            IEnumerable<NhanVien_TSCP> lst = _dbContext.GetAllRecords();
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }

        [HttpGet("GetDetailNhanVien_TSCP/{IdNhanVien}")]
        public async Task<IActionResult> GetDetailNhanVien_TSCP(int IdNhanVien)
        {
            NhanVien_TSCP_DetailViewModel obj = await _dbContext.GetDetailNhanVien_TSCP(IdNhanVien);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }

        [HttpGet("GetAllDetailNhanVien_TSCP")]
        public IActionResult GetAllDetailNhanVien_TSCP()
        {
            IEnumerable<NhanVien_TSCP_DetailViewModel> lst = (IEnumerable<NhanVien_TSCP_DetailViewModel>)_dbContext.GetAllDetailNhanVien_TSCP();
            if (lst == null)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }
    }
}
