﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuanLyNhanSu.Data.DataService;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Service.Controllers
{
    //api/KyLuat
    [Route("api/[controller]")]
    [ApiController]
    public class KyLuatController : ControllerBase
    {
        private readonly IKyLuatService _dbContext;

        public KyLuatController(IKyLuatService dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            var obj = await _dbContext.GetById(Id);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }


        [HttpPost("Create")]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> Create([FromForm] KyLuatViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Id = await _dbContext.Create(request);
            if (Id == 0)
                return BadRequest();

            var LichSuHopDong = await _dbContext.GetById(Id);

            return CreatedAtAction(nameof(GetById), new { id = Id }, LichSuHopDong);
        }

        //[HttpPut("{Id}")]
        //[Consumes("multipart/form-data")]
        //[Authorize]
        //public async Task<IActionResult> Update([FromRoute] int Id, [FromForm] LichSuHopDongViewModel request)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    LichSuHopDong obj = new LichSuHopDong()
        //    {
        //        Id = Id,
        //        MaHopDong = request.MaHopDong,
        //        IdLoaiHopDong = request.IdLoaiHopDong,
        //        IdNhanVien = request.IdNhanVien,
        //        NgayBatDau = request.NgayBatDau,
        //        NgayKetThuc = request.NgayKetThuc,
        //        NgayKy = request.NgayKy,
        //        TrangThai = request.TrangThai,
        //        NguoiKy = request.NguoiKy,
        //        NguoiTao = request.NguoiTao,
        //        LuongCB = request.LuongCB,
        //        IdHeSoLuong = request.IdHeSoLuong
        //    };
        //    var result = await _dbContext.Update(obj);
        //    if (result == 0)
        //        return BadRequest();
        //    return Ok();
        //}

        [HttpDelete("Delete/{Id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            var result = await _dbContext.Delete(Id);
            if (result == 0)
                return BadRequest();
            return Ok();
        }

        //[HttpGet("GetAllRecords")]
        //public IActionResult GetAllRecords()
        //{
        //    IEnumerable<KyLuat> lst = _dbContext.GetAllRecords();
        //    if (lst.Count() == 0)
        //        return BadRequest("Cannot find Obj");
        //    return Ok(lst);
        //}
        [HttpGet("GetListKyLuat/Thang={Thang}&&Nam={Nam}")]
        public IActionResult GetListKyLuat(int Thang, int Nam)
        {
            IEnumerable<KyLuatViewDetailModel> lst = (IEnumerable<KyLuatViewDetailModel>)_dbContext.GetListKyLuat(Thang, Nam);
            if (lst.Count() == 0)
                return BadRequest("Cannot find Obj");
            return Ok(lst);
        }

        [HttpGet("GetDetailKyLuatByNhanVien/IdNhanVien={Id}&&Thang={Thang}&&Nam={Nam}")]
        public IActionResult GetDetailKyLuatByNhanVien(int Id, int Thang, int Nam)
        {
            KyLuatViewDetailModel obj = _dbContext.GetDetailKyLuatByNhanVien(Id, Thang, Nam);
            if (obj == null)
                return BadRequest("Cannot find Obj");
            return Ok(obj);
        }

    }
}
