﻿using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Data.ViewModel.DM_ThamSoHeThong;
using QuanLyNhanSu.Data.ViewModel.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.ApiClient
{
    public interface IDM_BenhVienApiClient
    {

        Task<bool> Create(DM_ThamSoHeThongViewModel request);

        Task<bool> Update(DM_ThamSoHeThongViewModel request);

        Task<DM_ThamSoHeThong> GetById(int id, string languageId);

        Task<bool> Delete(int id);
    }
}