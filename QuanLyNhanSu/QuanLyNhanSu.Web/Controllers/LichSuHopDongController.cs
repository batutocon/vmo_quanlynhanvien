﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Web.Models;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using System.Net.Http.Headers;
using System.Globalization;
using QuanLyNhanSu.Data.ViewModel;

namespace QuanLyNhanSu.Web.Controllers
{
    public class LichSuHopDongController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public LichSuHopDongController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        // GET: NhanVienController
        public ActionResult Index()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/LichSuHopDong/GetAllRecords")).Result;

            var test = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<QuanLyNhanSu.Data.ViewModel.NhanVien_HopDongViewModel>();

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.NhanVien_HopDongViewModel>>(test.ToString());
                return View(listdata);
            }
            return View(listdata);
        }

        // GET: NhanVienController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: NhanVienController/Create
        public ActionResult Create(int id)
        {
            // lấy loại hợp đồng
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_LoaiHopDong/GetAllRecords")).Result;

            var test = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>();

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>>(test.ToString());
            }
            ViewBag.lstDM_HD = listdata;

            // lấy hệ số lương
            HttpResponseMessage response3 = Task.Run(async () => await client.GetAsync("/api/HeSoLuong/GetAllRecords")).Result;
            var test3 = response3.Content.ReadAsStringAsync().Result;
            var listdata3 = new List<QuanLyNhanSu.Model.Entities.HeSoLuong>();

            if (response.IsSuccessStatusCode)
            {
                listdata3 = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.HeSoLuong>>(test3.ToString());
            }
            ViewBag.lstHSL = listdata3;

            // lấy nhân viên
            NhanVien_HopDongViewModel obj = new NhanVien_HopDongViewModel();
            var client2 = _httpClientFactory.CreateClient();
            client2.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response2 = Task.Run(async () => await client2.GetAsync("/api/NhanVien/" + id)).Result;

            var test2 = response2.Content.ReadAsStringAsync().Result;
            var data2 = new QuanLyNhanSu.Model.Entities.NhanVien();

            if (response2.IsSuccessStatusCode)
            {
                data2 = JsonConvert.DeserializeObject<QuanLyNhanSu.Model.Entities.NhanVien>(test2.ToString());
            }
            obj.TenNV = data2.Ten;
            obj.Email = data2.Email;
            obj.IdNhanVien = data2.Id;
            obj.NgayBatDau = DateTime.Now;
            obj.NgayKetThuc = DateTime.Now;
            obj.NgayKy = DateTime.Now;

            return View(obj);
        }

        // POST: NhanVienController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(NhanVien_HopDongViewModel vm)
        {
            try
            {
                // lấy loại hợp đồng
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_LoaiHopDong/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>>(test.ToString());
                }
                ViewBag.lstDM_HD = listdata;

                // lấy hệ số lương
                HttpResponseMessage response3 = Task.Run(async () => await client.GetAsync("/api/HeSoLuong/GetAllRecords")).Result;
                var test3 = response3.Content.ReadAsStringAsync().Result;
                var listdata3 = new List<QuanLyNhanSu.Model.Entities.HeSoLuong>();

                if (response.IsSuccessStatusCode)
                {
                    listdata3 = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.HeSoLuong>>(test3.ToString());
                }
                ViewBag.lstHSL = listdata3;

                //tạo hợp đồng
                if (ModelState.IsValid)
                {
                    int checkNgayKyHopDong = DateTime.Compare(vm.NgayKy, vm.NgayBatDau);
                    int checkNgayHetHopDong = DateTime.Compare(vm.NgayBatDau, vm.NgayKetThuc);
                    if (checkNgayKyHopDong > 0)
                    {
                        TempData["error"] = "Ngày ký hợp đồng phải trước ngày bắt đầu";
                        return View(vm);
                    }
                    if (checkNgayHetHopDong > 0)
                    {
                        TempData["error"] = "Ngày bắt đầu phải muộn hơn ngày kết thúc";
                        return View(vm);
                    }
                    if (vm.IdHeSoLuong == 0)
                    {
                        TempData["error"] = "Chưa chọn loại hợp đồng";
                        return View(vm);
                    }
                    if (vm.LuongCB == 0 && vm.LuongCB % 100 != 0)
                    {
                        TempData["error"] = "Lương cơ bản phải chia hết cho 100";
                        return View(vm);
                    }
                    if (vm.IdLoaiHopDong == 0)
                    {
                        TempData["error"] = "Bạn chưa chọn loại hợp đồng";
                        return View(vm);
                    }

                    var createBy = User != null ? User.Identity.Name : "Admin";

                    var sessions = _httpContextAccessor
                  .HttpContext
                  .Session
                  .GetString(SystemConstants.AppSettings.Token);

                    var client2 = _httpClientFactory.CreateClient();
                    client2.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                    client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                    var requestContent = new MultipartFormDataContent();
                    requestContent.Add(new StringContent(vm.MaHopDong.ToString()), "MaHopDong");
                    requestContent.Add(new StringContent(vm.IdLoaiHopDong.ToString()), "IdLoaiHopDong");
                    requestContent.Add(new StringContent(vm.IdNhanVien.ToString()), "IdNhanVien");
                    requestContent.Add(new StringContent(vm.NgayBatDau.ToString()), "NgayBatDau");
                    requestContent.Add(new StringContent(vm.NgayKetThuc.ToString()), "NgayKetThuc");
                    requestContent.Add(new StringContent(vm.NgayKy.ToString()), "NgayKy");
                    requestContent.Add(new StringContent(vm.NguoiKy.ToString()), "NguoiKy");
                    requestContent.Add(new StringContent(vm.TrangThai.ToString()), "TrangThai");
                    requestContent.Add(new StringContent(createBy), "NguoiTao");
                    requestContent.Add(new StringContent(vm.LuongCB.ToString()), "LuongCB");
                    requestContent.Add(new StringContent(vm.IdHeSoLuong.ToString()), "IdHeSoLuong");
                    string url = string.Empty;
                    url = BASE_URL_API + "/api/LichSuHopDong";
                    var response2 = await client2.PostAsync(url, requestContent);
                    return RedirectToAction(nameof(Index));
                }
                return View(vm);
            }
            catch
            {
                return View();
            }
        }

        // GET: NhanVienController/Edit/5
        public ActionResult Edit(int id)
        {
            // lấy loại hợp đồng
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_LoaiHopDong/GetAllRecords")).Result;

            var test = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>();

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>>(test.ToString());
            }
            ViewBag.lstDM_HD = listdata;

            // lấy hệ số lương
            HttpResponseMessage response3 = Task.Run(async () => await client.GetAsync("/api/HeSoLuong/GetAllRecords")).Result;
            var test3 = response3.Content.ReadAsStringAsync().Result;
            var listdata3 = new List<QuanLyNhanSu.Model.Entities.HeSoLuong>();

            if (response.IsSuccessStatusCode)
            {
                listdata3 = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.HeSoLuong>>(test3.ToString());
            }
            ViewBag.lstHSL = listdata3;

            // lấy nhân viên
            var client2 = _httpClientFactory.CreateClient();
            client2.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response2 = Task.Run(async () => await client2.GetAsync("/api/LichSuHopDong/NhanVienHopDong/" + id)).Result;

            var test2 = response2.Content.ReadAsStringAsync().Result;
            var data2 = new NhanVien_HopDongViewModel();

            if (response2.IsSuccessStatusCode)
            {
                data2 = JsonConvert.DeserializeObject<NhanVien_HopDongViewModel>(test2.ToString());
            }

            return View(data2);
        }

        // POST: NhanVienController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(int id, NhanVien_HopDongViewModel vm)
        {
            try
            {
                // lấy loại hợp đồng
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_LoaiHopDong/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>>(test.ToString());
                }
                ViewBag.lstDM_HD = listdata;

                // lấy hệ số lương
                HttpResponseMessage response3 = Task.Run(async () => await client.GetAsync("/api/HeSoLuong/GetAllRecords")).Result;
                var test3 = response3.Content.ReadAsStringAsync().Result;
                var listdata3 = new List<QuanLyNhanSu.Model.Entities.HeSoLuong>();

                if (response.IsSuccessStatusCode)
                {
                    listdata3 = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.HeSoLuong>>(test3.ToString());
                }
                ViewBag.lstHSL = listdata3;

                //tạo hợp đồng
                if (ModelState.IsValid)
                {
                    int checkNgayKyHopDong = DateTime.Compare(vm.NgayKy, vm.NgayBatDau);
                    int checkNgayHetHopDong = DateTime.Compare(vm.NgayBatDau, vm.NgayKetThuc);
                    if (checkNgayKyHopDong > 0)
                    {
                        TempData["error"] = "Ngày ký hợp đồng phải trước ngày bắt đầu";
                        return View(vm);
                    }
                    if (checkNgayHetHopDong > 0)
                    {
                        TempData["error"] = "Ngày bắt đầu phải muộn hơn ngày kết thúc";
                        return View(vm);
                    }
                    if (vm.IdHeSoLuong == 0)
                    {
                        TempData["error"] = "Chưa chọn loại hợp đồng";
                        return View(vm);
                    }
                    if (vm.LuongCB == 0 && vm.LuongCB%100!=0)
                    {
                        TempData["error"] = "Lương cơ bản phải chia hết cho 100";
                        return View(vm);
                    }
                    if (vm.IdLoaiHopDong == 0)
                    {
                        TempData["error"] = "Bạn chưa chọn loại hợp đồng";
                        return View(vm);
                    }
                    var createBy = User != null ? User.Identity.Name : "Admin";

                    var sessions = _httpContextAccessor
                 .HttpContext
                 .Session
                 .GetString(SystemConstants.AppSettings.Token);

                    var client2 = _httpClientFactory.CreateClient();
                    client2.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                    client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                    var requestContent = new MultipartFormDataContent();
                    requestContent.Add(new StringContent(vm.MaHopDong.ToString()), "MaHopDong");
                    requestContent.Add(new StringContent(vm.IdLoaiHopDong.ToString()), "IdLoaiHopDong");
                    requestContent.Add(new StringContent(vm.IdNhanVien.ToString()), "IdNhanVien");
                    requestContent.Add(new StringContent(vm.NgayBatDau.ToString()), "NgayBatDau");
                    requestContent.Add(new StringContent(vm.NgayKetThuc.ToString()), "NgayKetThuc");
                    requestContent.Add(new StringContent(vm.NgayKy.ToString()), "NgayKy");
                    requestContent.Add(new StringContent(vm.NguoiKy.ToString()), "NguoiKy");
                    requestContent.Add(new StringContent(vm.TrangThai.ToString()), "TrangThai");
                    requestContent.Add(new StringContent(createBy), "NguoiTao");
                    requestContent.Add(new StringContent(vm.LuongCB.ToString()), "LuongCB");
                    requestContent.Add(new StringContent(vm.IdHeSoLuong.ToString()), "IdHeSoLuong");
                    string url = string.Empty;
                    url = BASE_URL_API + "/api/LichSuHopDong/" + id;
                    var response2 = await client2.PutAsync(url, requestContent);
                    return RedirectToAction(nameof(Index));
                }
                return View(vm);
            }
            catch
            {
                return View(vm);
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/LichSuHopDong/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }
    }
}
