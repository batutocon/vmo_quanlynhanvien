﻿using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using QuanLyNhanSu.Data.DataService;
using System;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using QuanLyNhanSu.Model.Entities;
using System.Net.Http.Headers;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_ChiNhanhNganHangController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_ChiNhanhNganHangController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_NganHang/GetAllRecords")).Result;
            var resultAPI = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<DM_NganHang>();
            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<DM_NganHang>>(resultAPI.ToString());
            }
            ViewBag.LISTNGANHANG = listdata;
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ChiNhanhNganHang/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_ChiNhanhNganHang>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_ChiNhanhNganHang>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string name, int idnganhang, string ghichu, int trangthai)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                HttpResponseMessage response_nh = Task.Run(async () => await client.GetAsync("/api/DM_NganHang/"+idnganhang)).Result;
                var test = response_nh.Content.ReadAsStringAsync().Result;
                var itemnganhang = new DM_NganHang();
                if (response_nh.IsSuccessStatusCode)
                {
                    itemnganhang = JsonConvert.DeserializeObject<DM_NganHang>(test.ToString());
                }
                DM_ChiNhanhNganHang obj = new DM_ChiNhanhNganHang();
                obj.TenChiNhanh = name.Trim();
                obj.MaNganHang = itemnganhang.Id.ToString();
                obj.TenNganHang = itemnganhang.TenNganHang ;
                obj.GhiChu = ghichu.Trim();
                obj.TrangThai = trangthai;
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);
               
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaChiNhanh");
                requestContent.Add(new StringContent(obj.TenChiNhanh.ToString()), "TenChiNhanh");
                requestContent.Add(new StringContent(obj.MaNganHang.ToString()), "MaNganHang");
                requestContent.Add(new StringContent(obj.TenNganHang.ToString()), "TenNganHang");
                requestContent.Add(new StringContent(obj.GhiChu.ToString()), "GhiChu");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ChiNhanhNganHang";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id,string ma, string name, int idnganhang, string ghichu, int trangthai)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response_nh = Task.Run(async () => await client.GetAsync("/api/DM_NganHang/" + idnganhang)).Result;
                var test = response_nh.Content.ReadAsStringAsync().Result;
                var itemnganhang = new DM_NganHang();
                if (response_nh.IsSuccessStatusCode)
                {
                    itemnganhang = JsonConvert.DeserializeObject<DM_NganHang>(test.ToString());
                }

                DM_ChiNhanhNganHang obj = new DM_ChiNhanhNganHang();
                obj.Id = id;
                obj.MaChiNhanh = ma;
                obj.TenChiNhanh = name.Trim();
                obj.MaNganHang = itemnganhang.Id.ToString();
                obj.TenNganHang = itemnganhang.TenNganHang;
                obj.GhiChu = ghichu.Trim();
                obj.TrangThai = trangthai;
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaChiNhanh.ToString()), "MaChiNhanh");
                requestContent.Add(new StringContent(obj.TenChiNhanh.ToString()), "TenChiNhanh");
                requestContent.Add(new StringContent(obj.MaNganHang.ToString()), "MaNganHang");
                requestContent.Add(new StringContent(obj.TenNganHang.ToString()), "TenNganHang");
                requestContent.Add(new StringContent(obj.GhiChu.ToString()), "GhiChu");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ChiNhanhNganHang/" + obj.Id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }

        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ChiNhanhNganHang/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}