﻿using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using QuanLyNhanSu.Data.DataService;
using System;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using QuanLyNhanSu.Model.Entities;
using System.Net.Http.Headers;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_ChucDanhController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_ChucDanhController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DonVi/GetAllRecords")).Result;

            var test = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<QuanLyNhanSu.Model.Entities.DonVi>();

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DonVi>>(test.ToString());
            }
            ViewBag.lstDV = listdata;
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ChucDanh/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<DonVi_ChucDanhViewModel>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<DonVi_ChucDanhViewModel>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string name,string desc, string madonvi)
        {
            try
            {
                if (name.Length == 0 || desc.Length == 0)
                {
                    return Json("Không được để trống");
                }

                if (madonvi == "0")
                {
                    return Json("Bạn chưa chọn đơn vị");
                }              
            
                QuanLyNhanSu.Model.Entities.DM_ChucDanh obj = new QuanLyNhanSu.Model.Entities.DM_ChucDanh();
                obj.TenChucDanh = name.Trim();
                obj.MoTa = desc.Trim();
                obj.IdDonVi = madonvi.Trim();

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaChucDanh");
                requestContent.Add(new StringContent(obj.TenChucDanh.ToString()), "TenChucDanh");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                requestContent.Add(new StringContent(obj.IdDonVi.ToString()), "IdDonVi");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ChucDanh";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string name,string ma, string desc, string madonvi)
        {
            try
            {
                if (name.Length == 0 || desc.Length == 0)
                {
                    return Json("Không được để trống");
                }

                if (madonvi == "0")
                {
                    return Json("Bạn chưa chọn đơn vị");
                }

                QuanLyNhanSu.Model.Entities.DM_ChucDanh obj = new QuanLyNhanSu.Model.Entities.DM_ChucDanh();
                obj.MaChucDanh = ma.Trim();
                obj.TenChucDanh = name.Trim();
                obj.MoTa = desc.Trim();
                obj.IdDonVi = madonvi.Trim();


                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaChucDanh.ToString()), "MaChucDanh");
                requestContent.Add(new StringContent(obj.TenChucDanh.ToString()), "TenChucDanh");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                requestContent.Add(new StringContent(obj.IdDonVi.ToString()), "IdDonVi");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ChucDanh/" + id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }

        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ChucDanh/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}