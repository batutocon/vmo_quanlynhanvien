﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class NhanVien_TSCPController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public NhanVien_TSCPController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/NhanVien_TSCP/GetAllDetailNhanVien_TSCP")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<NhanVien_TSCP_DetailViewModel>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<NhanVien_TSCP_DetailViewModel>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }
        public ActionResult GetListTSCP(int id)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/NhanVien_TSCP/GetDetailNhanVien_TSCP/"+id)).Result;

                var data = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<TaiSanCapPhatViewModel>();

                HttpResponseMessage response1 = Task.Run(async () => await client.GetAsync("/api/DM_TaiSanCapPhat/GetAllRecords")).Result;
                var data1 = response1.Content.ReadAsStringAsync().Result;
                var lst_ts = new List<QuanLyNhanSu.Model.Entities.DM_TaiSanCapPhat>();
                if (response.IsSuccessStatusCode)
                {
                    lst_ts = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TaiSanCapPhat>>(data1.ToString());
                }

                if (response.IsSuccessStatusCode)
                {
                    NhanVien_TSCP_DetailViewModel csdl = JsonConvert.DeserializeObject<NhanVien_TSCP_DetailViewModel>(data.ToString());
                    foreach (var item in lst_ts)
                    {
                        var obj = new TaiSanCapPhatViewModel();
                        obj.Id = item.Id;
                        obj.MaTaiSanCapPhat = item.MaTaiSanCapPhat;
                        obj.TenTaiSanCapPhat = item.TenTaiSanCapPhat;
                        obj.MoTa = item.MoTa;
                        obj.SoLuongConLai = item.SoLuong;
                        obj.SoLuong = 0;
                        if (csdl.dM_TaiSanCapPhats!=null) {
                            foreach (var itemDaMuon in csdl.dM_TaiSanCapPhats)
                            {
                                if (itemDaMuon.Id == item.Id)
                                {
                                    obj.SoLuong = itemDaMuon.SoLuong;
                                }
                            }
                        }                        
                        listdata.Add(obj);
                    }
                    return PartialView("_PartialDataDMTSCP", listdata);
                }

                return PartialView("_PartialDataDMTSCP", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialDataDMTSCP", null);
            }
        }

        public async Task<JsonResult> CapNhat(int id, string id_tscp, string noidung,string type)
        {
            try
            {
                if (noidung==null)
                {
                    noidung = "";
                }
                var nguoitao = User != null ? User.Identity.Name : "Admin";

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);

                HttpResponseMessage rp = Task.Run(async () => await client.GetAsync("/api/DM_ThamSoHeThong/" + id)).Result;
                var result_obj = rp.Content.ReadAsStringAsync().Result;
               
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(id_tscp.ToString().Trim()), "IdTSCP");
                requestContent.Add(new StringContent(SystemConstants.Web.TRANGTHAI_NHANVIEN_TSCP_DANGCHOMUON.ToString().Trim()), "TrangThai");
                requestContent.Add(new StringContent(noidung.ToString().Trim()), "NoiDung");
                string url = string.Empty;
                if (type.Equals("Create"))
                {
                    requestContent.Add(new StringContent(id.ToString().Trim()), "IdNhanVien");
                    requestContent.Add(new StringContent(nguoitao), "NguoiTao");
                    url = BASE_URL_API + "/api/NhanVien_TSCP/Create";
                    HttpResponseMessage response = Task.Run(async () => await client.PostAsync(url, requestContent)).Result;
                }
                else
                {
                    requestContent.Add(new StringContent(id.ToString().Trim()), "Id");
                    requestContent.Add(new StringContent(nguoitao), "NguoiCapNhat");
                    url = BASE_URL_API + "/api/NhanVien_TSCP/Update";
                    HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;
                }              

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
    }
}
