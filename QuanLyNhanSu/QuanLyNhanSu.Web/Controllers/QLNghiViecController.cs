﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using QuanLyNhanSu.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class QLNghiViecController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public QLNghiViecController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/QLNghiViec/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QLNghiViecGetAllViewModel>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QLNghiViecGetAllViewModel>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }
        public async Task<JsonResult> CapNhat(int id, string note, int trangthai)
        {
            try
            {                
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(note.Trim()), "note");
                requestContent.Add(new StringContent(trangthai.ToString()), "trangthai");
                //      var response = await client.PutAsync($"​/api​/DM_QuocGia/" + obj.Id, requestContent);
                string url = string.Empty;
                url = BASE_URL_API + "/api/QLNghiViec/"+id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/QLNghiViec/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }
    }
}
