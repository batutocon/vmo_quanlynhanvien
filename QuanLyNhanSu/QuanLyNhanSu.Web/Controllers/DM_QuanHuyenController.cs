﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_QuanHuyenController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_QuanHuyenController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_QuocGia/GetAllRecords")).Result;

            var test = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<QuanLyNhanSu.Model.Entities.DM_QuocGia>();

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_QuocGia>>(test.ToString());
            }
            ViewBag.lstQG = listdata;
            return View();
        }

        public JsonResult LoadListCity(string maquocgia)
        {
            string str = "";
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_TinhThanh/GetListTinhThanhByQuocGia/" + maquocgia.Trim())).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listData = new List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>();

                if (response.IsSuccessStatusCode)
                {
                    listData = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>>(test.ToString());
                }


                if (listData.Count > 0)
                {
                    str += "<option value=\"0\">Chọn Tỉnh thành</option>";
                    foreach (var item in listData)
                    {
                        str += "<option value=\"" + item.MaTinhThanh + "\">" + item.TenTinhThanh + "</option>";
                    }
                }
            }
            catch (Exception)
            {
            }
            return Json(str);
        }

        public ActionResult GetData(string matinhthanh)
        {
            try
            {
                if (matinhthanh.Equals("0"))
                {
                    return PartialView("_PartialData", null);
                }
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_QuanHuyen/GetListQuanHuyenByTinhThanh/" + matinhthanh.Trim())).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_QuanHuyen>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_QuanHuyen>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string name, string matinhthanh, int trangthai)
        {
            try
            {
                if (name.Length == 0)
                {
                    return Json("Không được để trống");
                }
                if (matinhthanh.Length == 0)
                {
                    return Json("Bạn chưa chọn Tỉnh thành");
                }
                if (trangthai != 0 && trangthai != 1)
                {
                    return Json("Trạng thái không hợp lệ");
                }
                QuanLyNhanSu.Model.Entities.DM_QuanHuyen obj = new QuanLyNhanSu.Model.Entities.DM_QuanHuyen();
                obj.MaTinhThanh = matinhthanh.Trim();
                obj.TenQuanHuyen = name.Trim();
                obj.TrangThai = trangthai;

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(matinhthanh.Trim()), "MaTinhThanh");
                requestContent.Add(new StringContent("123"), "MaQuanHuyen");
                requestContent.Add(new StringContent(obj.TenQuanHuyen.ToString()), "TenQuanHuyen");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_QuanHuyen";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string ma, string matinhthanh, string name, int trangthai)
        {
            try
            {
                if (name.Length == 0 || ma.Length == 0)
                {
                    return Json("Không được để trống");
                }
                if (matinhthanh.Length == 0)
                {
                    return Json("Bạn chưa chọn Tỉnh thành");
                }
                if (trangthai != 0 && trangthai != 1)
                {
                    return Json("Trạng thái không hợp lệ");
                }
                QuanLyNhanSu.Model.Entities.DM_QuanHuyen obj = new QuanLyNhanSu.Model.Entities.DM_QuanHuyen();
                obj.Id = id;
                obj.MaTinhThanh = matinhthanh.Trim();
                obj.MaQuanHuyen = ma.Trim();
                obj.TenQuanHuyen = name.Trim();
                obj.TrangThai = trangthai;

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaTinhThanh.ToString()), "MaTinhThanh");
                requestContent.Add(new StringContent(obj.MaQuanHuyen.ToString()), "MaQuanHuyen");
                requestContent.Add(new StringContent(obj.TenQuanHuyen.ToString()), "TenQuanHuyen");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                //      var response = await client.PutAsync($"​/api​/DM_QuocGia/" + obj.Id, requestContent);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_QuanHuyen/" + obj.Id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_QuanHuyen/" + id;
                // HttpResponseMessage response = await client.DeleteAsync(url);
                HttpResponseMessage response = Task.Run(async () => await client.DeleteAsync(url)).Result;

                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }

}
