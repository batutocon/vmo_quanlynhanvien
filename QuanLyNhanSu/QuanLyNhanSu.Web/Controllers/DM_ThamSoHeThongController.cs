﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_ThamSoHeThongController : Controller
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_ThamSoHeThongController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ThamSoHeThong/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_ThamSoHeThong>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_ThamSoHeThong>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string mathamso,string tenthamso,string mota,string giatri,int trangthai)
        {
            try
            {
                DM_ThamSoHeThong obj = new DM_ThamSoHeThong();
                obj.MaThamSo = mathamso.Trim();
                obj.TenThamSo = tenthamso.Trim();
                obj.MoTa = mota.Trim();
                obj.GiaTri = giatri.Trim();
                obj.NgayTao = DateTime.Now;
                obj.NgayCapNhat = DateTime.Now;
                obj.TrangThai = trangthai;
                obj.NguoiTao = User != null ? User.Identity.Name : "Admin";
                obj.NguoiCapNhat = User != null ? User.Identity.Name : "Admin";
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();

                requestContent.Add(new StringContent(obj.MaThamSo), "MaThamSo");
                requestContent.Add(new StringContent(obj.TenThamSo.ToString()), "TenThamSo");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                requestContent.Add(new StringContent(obj.GiaTri.ToString()), "GiaTri");
                requestContent.Add(new StringContent(obj.NgayTao.ToString()), "NgayTao");
                requestContent.Add(new StringContent(obj.NgayCapNhat.ToString()), "NgayCapNhat");
                requestContent.Add(new StringContent(obj.NguoiTao.ToString()), "NguoiTao");
                requestContent.Add(new StringContent(obj.NguoiCapNhat.ToString()), "NguoiCapNhat");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ThamSoHeThong";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string mathamso, string tenthamso, string mota, string giatri, int trangthai)
        {
            try
            {

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);

                HttpResponseMessage rp = Task.Run(async () => await client.GetAsync("/api/DM_ThamSoHeThong/"+id)).Result;
                var result_obj = rp.Content.ReadAsStringAsync().Result;
                var obj = JsonConvert.DeserializeObject<DM_ThamSoHeThong>(result_obj.ToString());

                obj.MaThamSo = mathamso.Trim();
                obj.TenThamSo = tenthamso.Trim();
                obj.MoTa = mota.Trim();
                obj.GiaTri = giatri.Trim();
                obj.NgayCapNhat = DateTime.Now;
                obj.NguoiCapNhat = User != null ? User.Identity.Name : "Admin";
                obj.TrangThai = trangthai;

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaThamSo), "MaThamSo");
                requestContent.Add(new StringContent(obj.TenThamSo.ToString()), "TenThamSo");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                requestContent.Add(new StringContent(obj.GiaTri.ToString()), "GiaTri");
                requestContent.Add(new StringContent(obj.NgayTao.ToString()), "NgayTao");
                requestContent.Add(new StringContent(obj.NgayCapNhat.ToString()), "NgayCapNhat");
                requestContent.Add(new StringContent(obj.NguoiTao.ToString()), "NguoiTao");
                requestContent.Add(new StringContent(obj.NguoiCapNhat.ToString()), "NguoiCapNhat");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ThamSoHeThong/" + id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_ThamSoHeThong/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}

