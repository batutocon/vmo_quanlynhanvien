﻿using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using QuanLyNhanSu.Data.DataService;
using System;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using QuanLyNhanSu.Model.Entities;
using System.Net.Http.Headers;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_LoaiHopDongController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_LoaiHopDongController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_LoaiHopDong/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_LoaiHopDong>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string name,string tghd,string ghichu, int trangthai)
        {
            try
            {
                DM_LoaiHopDong obj = new DM_LoaiHopDong();
                obj.TenLoaiHopDong = name.Trim();
                obj.TrangThai = trangthai;
                obj.ThoiHanHopDong = tghd.Trim();
                obj.GhiChu = ghichu.Trim();

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaLoaiHopDong");
                requestContent.Add(new StringContent(obj.TenLoaiHopDong.ToString()), "TenLoaiHopDong");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                requestContent.Add(new StringContent(obj.ThoiHanHopDong.ToString()), "ThoiHanHopDong");
                requestContent.Add(new StringContent(obj.GhiChu.ToString()), "GhiChu");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_LoaiHopDong";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua (int id, string name, string ma, string tghd, string ghichu, int trangthai)
        {
            try
            {
                DM_LoaiHopDong obj = new DM_LoaiHopDong();
                obj.Id = id;
                obj.MaLoaiHopDong = ma;
                obj.TenLoaiHopDong = name.Trim();
                obj.TrangThai = trangthai;
                obj.ThoiHanHopDong = tghd.Trim();
                obj.GhiChu = ghichu.Trim();

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaLoaiHopDong), "MaLoaiHopDong");
                requestContent.Add(new StringContent(obj.TenLoaiHopDong.ToString()), "TenLoaiHopDong");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                requestContent.Add(new StringContent(obj.ThoiHanHopDong.ToString()), "ThoiHanHopDong");
                requestContent.Add(new StringContent(obj.GhiChu.ToString()), "GhiChu");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_LoaiHopDong/"+id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_LoaiHopDong/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}