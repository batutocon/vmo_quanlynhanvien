﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class KhenThuongController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public KhenThuongController(IHttpClientFactory httpClientFactory,
                       IHttpContextAccessor httpContextAccessor,
                        IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult GetData(int? Thang = 0, int? Nam = 0)
        {
            try
            {
                if(Thang == 0 || Nam == 0)
                {
                    return PartialView("_PartialData", null);
                }
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/KhenThuong/GetListKhenThuong/Thang="+Thang+"&&Nam="+Nam)).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<KhenThuongViewDetailModel>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<KhenThuongViewDetailModel>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(int Id, string Noidung, double Sotien, int Thang, int Nam)
        {
            try
            {
                var nguoitao = User != null ? User.Identity.Name : "Admin";
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(Id.ToString()), "IdNhanVien");
                requestContent.Add(new StringContent(Noidung.ToString()), "NoiDungKhenThuong");
                requestContent.Add(new StringContent(Sotien.ToString()), "SoTien");
                requestContent.Add(new StringContent(Thang.ToString()), "Thang");
                requestContent.Add(new StringContent(Nam.ToString()), "Nam");
                requestContent.Add(new StringContent(nguoitao.ToString()), "NguoiTao");
                string url = string.Empty;
                url = BASE_URL_API + "/api/KhenThuong/Create";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }
    }
}
