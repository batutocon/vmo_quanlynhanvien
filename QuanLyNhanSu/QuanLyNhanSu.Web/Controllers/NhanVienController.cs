﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Web.Models;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using System.Net.Http.Headers;
using System.Globalization;

namespace QuanLyNhanSu.Web.Controllers
{
    public class NhanVienController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public NhanVienController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        // GET: NhanVienController
        public ActionResult Index()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/NhanVien/GetAllRecords")).Result;

            var test = response.Content.ReadAsStringAsync().Result;
            var listdata = new List<QuanLyNhanSu.Data.ViewModel.ViewData.NhanVienViewModel>();

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.ViewData.NhanVienViewModel>>(test.ToString());
                return View(listdata);
            }
            return View(listdata);
        }

        // GET: NhanVienController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: NhanVienController/Create
        public ActionResult Create()
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ChucDanh/GetAllRecords")).Result;
            HttpResponseMessage response2 = Task.Run(async () => await client.GetAsync("/api/PhongBan/GetAllRecords")).Result;
            HttpResponseMessage response4 = Task.Run(async () => await client.GetAsync("/api/DM_TinhThanh/GetAllRecords")).Result;

            var chucdanhString = response.Content.ReadAsStringAsync().Result;
            var listdataCD = new List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>();
            var phonngbanString = response2.Content.ReadAsStringAsync().Result;
            var listdataPB = new List<QuanLyNhanSu.Model.Entities.PhongBan>();
            var tinhthanhString = response4.Content.ReadAsStringAsync().Result;
            var listdataTT = new List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>();

            if (response.IsSuccessStatusCode)
            {
                listdataCD = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>>(chucdanhString.ToString());
            }
            if (response2.IsSuccessStatusCode)
            {
                listdataPB = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.PhongBan>>(phonngbanString.ToString());
            }
            if (response4.IsSuccessStatusCode)
            {
                listdataTT = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>>(tinhthanhString.ToString());
            }
            ViewBag.lstDM_CD = listdataCD;
            ViewBag.lstDM_PB = listdataPB;
            ViewBag.lstDM_TT = listdataTT;

            return View();
        }

        // POST: NhanVienController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(NhanVienViewModel vm)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ChucDanh/GetAllRecords")).Result;
                HttpResponseMessage response2 = Task.Run(async () => await client.GetAsync("/api/PhongBan/GetAllRecords")).Result;
                HttpResponseMessage response4 = Task.Run(async () => await client.GetAsync("/api/DM_TinhThanh/GetAllRecords")).Result;

                var chucdanhString = response.Content.ReadAsStringAsync().Result;
                var listdataCD = new List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>();
                var phonngbanString = response2.Content.ReadAsStringAsync().Result;
                var listdataPB = new List<QuanLyNhanSu.Model.Entities.PhongBan>();
                var tinhthanhString = response4.Content.ReadAsStringAsync().Result;
                var listdataTT = new List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>();

                if (response.IsSuccessStatusCode)
                {
                    listdataCD = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>>(chucdanhString.ToString());
                }
                if (response2.IsSuccessStatusCode)
                {
                    listdataPB = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.PhongBan>>(phonngbanString.ToString());
                }
                if (response4.IsSuccessStatusCode)
                {
                    listdataTT = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>>(tinhthanhString.ToString());
                }
                ViewBag.lstDM_CD = listdataCD;
                ViewBag.lstDM_PB = listdataPB;
                ViewBag.lstDM_TT = listdataTT;

                if (ModelState.IsValid)
                {
                    var createBy = User != null ? User.Identity.Name : "Admin";

                    var sessions = _httpContextAccessor
                   .HttpContext
                   .Session
                   .GetString(SystemConstants.AppSettings.Token);

                    var client3 = _httpClientFactory.CreateClient();
                    client3.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                    client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                    var requestContent = new MultipartFormDataContent();
                    requestContent.Add(new StringContent("133"), "IdCBNV");
                    requestContent.Add(new StringContent(vm.Ten.ToString()), "Ten");
                    requestContent.Add(new StringContent(vm.GioiTinh.ToString()), "GioiTinh");
                    requestContent.Add(new StringContent(vm.NgaySinh.ToString()), "NgaySinh");
                    requestContent.Add(new StringContent(vm.DiaChi.ToString()), "DiaChi");
                    requestContent.Add(new StringContent(vm.CanCuoc_CMND.ToString()), "CanCuoc_CMND");
                    requestContent.Add(new StringContent(vm.NgayCap.ToString()), "NgayCap");
                    requestContent.Add(new StringContent(vm.NoiCap.ToString()), "NoiCap");
                    requestContent.Add(new StringContent(vm.Email.ToString()), "Email");
                    requestContent.Add(new StringContent(vm.ChucDanh.ToString()), "ChucVu");
                    requestContent.Add(new StringContent(vm.PhongBan.ToString()), "PhongBan");
                    requestContent.Add(new StringContent(vm.TrangThai.ToString()), "TrangThai");
                    requestContent.Add(new StringContent(createBy), "NguoiTao");
                    string url = string.Empty;
                    url = BASE_URL_API + "/api/NhanVien";
                    var response3 = await client3.PostAsync(url, requestContent);
                    return RedirectToAction(nameof(Index));

                }
                return View(vm);
            }
            catch
            {
                return View();
            }
        }

        // GET: NhanVienController/Edit/5
        public ActionResult Edit(int id)
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ChucDanh/GetAllRecords")).Result;
            HttpResponseMessage response2 = Task.Run(async () => await client.GetAsync("/api/PhongBan/GetAllRecords")).Result;
            HttpResponseMessage response4 = Task.Run(async () => await client.GetAsync("/api/DM_TinhThanh/GetAllRecords")).Result;

            var chucdanhString = response.Content.ReadAsStringAsync().Result;
            var listdataCD = new List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>();
            var phonngbanString = response2.Content.ReadAsStringAsync().Result;
            var listdataPB = new List<QuanLyNhanSu.Model.Entities.PhongBan>();
            var tinhthanhString = response4.Content.ReadAsStringAsync().Result;
            var listdataTT = new List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>();

            if (response.IsSuccessStatusCode)
            {
                listdataCD = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>>(chucdanhString.ToString());
            }
            if (response2.IsSuccessStatusCode)
            {
                listdataPB = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.PhongBan>>(phonngbanString.ToString());
            }
            if (response4.IsSuccessStatusCode)
            {
                listdataTT = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>>(tinhthanhString.ToString());
            }
            ViewBag.lstDM_CD = listdataCD;
            ViewBag.lstDM_PB = listdataPB;
            ViewBag.lstDM_TT = listdataTT;


            HttpResponseMessage response3 = Task.Run(async () => await client.GetAsync("/api/NhanVien/"+id)).Result;

            var test3 = response3.Content.ReadAsStringAsync().Result;
            var data3 = new QuanLyNhanSu.Model.Entities.NhanVien();

            if (response3.IsSuccessStatusCode)
            {
                data3 = JsonConvert.DeserializeObject<QuanLyNhanSu.Model.Entities.NhanVien>(test3.ToString());
            }
            var obj = new NhanVienViewModel();
            obj.Ten = data3.Ten;
            obj.CanCuoc_CMND = data3.CanCuoc_CMND;
            obj.DiaChi = data3.DiaChi;
            obj.Email = data3.Email;
            obj.GioiTinh = data3.GioiTinh;
            obj.NgayCap = data3.NgayCap;
            obj.NgaySinh = data3.NgaySinh;
            obj.NoiCap = data3.NoiCap;
            obj.ChucDanh = data3.ChucVu;
            obj.PhongBan = data3.PhongBan;
            obj.TrangThai = data3.TrangThai;
            return View(obj);
        }

        // POST: NhanVienController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(int id ,NhanVienViewModel vm)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_ChucDanh/GetAllRecords")).Result;
                HttpResponseMessage response2 = Task.Run(async () => await client.GetAsync("/api/PhongBan/GetAllRecords")).Result;
                HttpResponseMessage response4 = Task.Run(async () => await client.GetAsync("/api/DM_TinhThanh/GetAllRecords")).Result;

                var chucdanhString = response.Content.ReadAsStringAsync().Result;
                var listdataCD = new List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>();
                var phonngbanString = response2.Content.ReadAsStringAsync().Result;
                var listdataPB = new List<QuanLyNhanSu.Model.Entities.PhongBan>();
                var tinhthanhString = response4.Content.ReadAsStringAsync().Result;
                var listdataTT = new List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>();

                if (response.IsSuccessStatusCode)
                {
                    listdataCD = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.DonVi_ChucDanhViewModel>>(chucdanhString.ToString());
                }
                if (response2.IsSuccessStatusCode)
                {
                    listdataPB = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.PhongBan>>(phonngbanString.ToString());
                }
                if (response4.IsSuccessStatusCode)
                {
                    listdataTT = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TinhThanh>>(tinhthanhString.ToString());
                }
                ViewBag.lstDM_CD = listdataCD;
                ViewBag.lstDM_PB = listdataPB;
                ViewBag.lstDM_TT = listdataTT;

                if (ModelState.IsValid)
                {
                    var createBy = User != null ? User.Identity.Name : "Admin";

                    var sessions = _httpContextAccessor
                   .HttpContext
                   .Session
                   .GetString(SystemConstants.AppSettings.Token);

                    var client3 = _httpClientFactory.CreateClient();
                    client3.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                    client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                    var requestContent = new MultipartFormDataContent();
                    requestContent.Add(new StringContent("133"), "IdCBNV");
                    requestContent.Add(new StringContent(vm.Ten.ToString()), "Ten");
                    requestContent.Add(new StringContent(vm.GioiTinh.ToString()), "GioiTinh");
                    requestContent.Add(new StringContent(vm.NgaySinh.ToString()), "NgaySinh");
                    requestContent.Add(new StringContent(vm.DiaChi.ToString()), "DiaChi");
                    requestContent.Add(new StringContent(vm.CanCuoc_CMND.ToString()), "CanCuoc_CMND");
                    requestContent.Add(new StringContent(vm.NgayCap.ToString()), "NgayCap");
                    requestContent.Add(new StringContent(vm.NoiCap.ToString()), "NoiCap");
                    requestContent.Add(new StringContent(vm.Email.ToString()), "Email");
                    requestContent.Add(new StringContent(vm.ChucDanh.ToString()), "ChucVu");
                    requestContent.Add(new StringContent(vm.PhongBan.ToString()), "PhongBan");
                    requestContent.Add(new StringContent(vm.TrangThai.ToString()), "TrangThai");
                    requestContent.Add(new StringContent(createBy), "NguoiTao");
                    string url = string.Empty;
                    url = BASE_URL_API + "/api/NhanVien/"+ id;
                    var response3 = await client3.PutAsync(url, requestContent);
                    return RedirectToAction(nameof(Index));
                }
                return View(vm);
            }
            catch
            {
                return View();
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/NhanVien/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }
    }
}
