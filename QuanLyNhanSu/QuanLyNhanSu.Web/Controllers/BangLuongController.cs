﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Web.Models;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using System.Net.Http.Headers;
using System.Globalization;
using QuanLyNhanSu.Data.ViewModel;

namespace QuanLyNhanSu.Web.Controllers
{
    public class BangLuongController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public BangLuongController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        // GET: NhanVienController
        public ActionResult Index(int thang = 0, int nam = 0)
        {
            var listdata = new List<QuanLyNhanSu.Data.ViewModel.BangLuongTheoThangNam>();
            ViewBag.Thang = thang;
            ViewBag.Nam = nam;
            if (thang == 0 || nam == 0)
            {
                return View(listdata);
            }
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/BangLuong/GetListBangLuongTheoThangNam/"+"Thang="+thang+"&&Nam="+nam)).Result;

            var test = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Data.ViewModel.BangLuongTheoThangNam>>(test.ToString());                
                return View(listdata);
            }           
            return View(listdata);
        }

        // GET: NhanVienController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        
        public async Task<JsonResult> SuaTrangThai(int id, int trangthai)
        {
            try
            {               
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(id.ToString().Trim()), "Id"); ;
                requestContent.Add(new StringContent(trangthai.ToString().Trim()), "TrangThai");                
                string url = string.Empty;
                url = BASE_URL_API + "/api/BangLuong/NhanLuong";
                var response = await client.PutAsync(url, requestContent);

                TempData["message"] = "Cập nhật thành công";
                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi " + ex.Message;
                return Json("Lỗi");
            }
        }

        public async Task<JsonResult> TaoBangLuong (int idnv, int thang, int nam, int idhopdong)
        {
            try
            {
                if (thang == 0 || nam== 0)
                {
                    return Json("Bạn chưa chọn tháng và năm");
                }
                var nguoitao =  User != null ? User.Identity.Name : "Admin";

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(idnv.ToString().Trim()), "IdNhanVien"); ;
                requestContent.Add(new StringContent(thang.ToString().Trim()), "Thang"); ;
                requestContent.Add(new StringContent(nam.ToString().Trim()), "Nam"); ;
                requestContent.Add(new StringContent(idhopdong.ToString().Trim()), "IdLichSuHopDong"); ;
                requestContent.Add(new StringContent(nguoitao), "NguoiTao"); ;
                requestContent.Add(new StringContent(DateTime.Now.ToString()), "NgayTao");
                requestContent.Add(new StringContent(1.ToString()), "TrangThai");
                string url = string.Empty;
                url = BASE_URL_API + "/api/BangLuong";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> TinhLuong(int bangluongId, double luongCB, int heso)
        {
            try
            {
                var nguoitao = User != null ? User.Identity.Name : "Admin";

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(bangluongId.ToString().Trim()), "Id"); ;
                requestContent.Add(new StringContent(luongCB.ToString().Trim()), "luongCB"); ;
                requestContent.Add(new StringContent(heso.ToString().Trim()), "heso"); ;
                string url = string.Empty;
                url = BASE_URL_API + "/api/BangLuong/TinhLuong";
                var response = await client.PutAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi: " + ex.Message;
                return Json("Lỗi");
            }
        }

        public async Task<JsonResult> ThuongPhat(int bangluongId)
        {
            try
            {
                var nguoitao = User != null ? User.Identity.Name : "Admin";

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(bangluongId.ToString().Trim()), "Id"); ;
                string url = string.Empty;
                url = BASE_URL_API + "/api/BangLuong/ThuongPhat";
                var response = await client.PutAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi: " + ex.Message;
                return Json("Lỗi");
            }
        }
    }
}
