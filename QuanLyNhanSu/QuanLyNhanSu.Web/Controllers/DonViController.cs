﻿using System.Linq;
using System.Threading.Tasks;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Data.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using QuanLyNhanSu.Data.DataService;
using System;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DonViController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DonViController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DonVi/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DonVi>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DonVi>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string name, string mota="")
        {
            try
            {
                if (name.Length == 0)
                {
                    return Json("Không được để trống");
                }
                if (mota == null)
                {
                    mota = "";
                }

                QuanLyNhanSu.Model.Entities.DonVi obj = new QuanLyNhanSu.Model.Entities.DonVi();
                obj.TenDonVi = name.Trim();
                obj.MoTa = mota.Trim();

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaDonVi");
                requestContent.Add(new StringContent(obj.TenDonVi.ToString()), "TenDonVi");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DonVi";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string ma, string name, string mota="")
        {
            try
            {
                if (name.Length == 0 || ma.Length == 0)
                {
                    return Json("Không được để trống");
                }
                if (mota == null)
                {
                    mota = "";
                }

                QuanLyNhanSu.Model.Entities.DonVi obj = new QuanLyNhanSu.Model.Entities.DonVi();
                obj.Id = id;
                obj.MaDonVi = ma.Trim();
                obj.TenDonVi = name.Trim();
                obj.MoTa = mota.Trim();

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaDonVi.ToString()), "MaDonVi");
                requestContent.Add(new StringContent(obj.TenDonVi.ToString()), "TenDonVi");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                //      var response = await client.PutAsync($"​/api​/DonVi/" + obj.Id, requestContent);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DonVi/" + obj.Id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DonVi/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}