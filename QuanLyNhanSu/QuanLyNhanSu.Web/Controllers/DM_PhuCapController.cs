﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_PhuCapController : Controller
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_PhuCapController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_PhuCap/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_PhuCap>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_PhuCap>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string loaithuong,string nguonchi,float sotienluongcb, string ghichu, int trangthai)
        {
            try
            {
                DM_PhuCap obj = new DM_PhuCap();
                obj.LoaiThuong = loaithuong.Trim();
                obj.NguonChi = nguonchi.Trim();
                obj.SoTienTheoLuongCB = sotienluongcb;
                obj.TrangThai = trangthai;
                obj.GhiChu = ghichu.Trim();
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaPhuCap");
                requestContent.Add(new StringContent(obj.LoaiThuong.ToString()), "LoaiThuong");
                requestContent.Add(new StringContent(obj.NguonChi.ToString()), "NguonChi");
                requestContent.Add(new StringContent(obj.SoTienTheoLuongCB.ToString()), "SoTienTheoLuongCB");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                requestContent.Add(new StringContent(obj.GhiChu.ToString()), "GhiChu");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_PhuCap";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string ma, string loaithuong, string nguonchi, float sotienluongcb, string ghichu, int trangthai)
        {
            try
            {
                DM_PhuCap obj = new DM_PhuCap();
                obj.Id = id;
                obj.MaPhuCap = ma;
                obj.LoaiThuong = loaithuong.Trim();
                obj.NguonChi = nguonchi.Trim();
                obj.SoTienTheoLuongCB = sotienluongcb;
                obj.TrangThai = trangthai;
                obj.GhiChu = ghichu.Trim();

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaPhuCap), "MaPhuCap");
                requestContent.Add(new StringContent(obj.LoaiThuong.ToString()), "LoaiThuong");
                requestContent.Add(new StringContent(obj.NguonChi.ToString()), "NguonChi");
                requestContent.Add(new StringContent(obj.SoTienTheoLuongCB.ToString()), "SoTienTheoLuongCB");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                requestContent.Add(new StringContent(obj.GhiChu.ToString()), "GhiChu");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_PhuCap/" + id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_PhuCap/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}