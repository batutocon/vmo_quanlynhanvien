﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_PhucLoiController : Controller
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_PhucLoiController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_PhucLoi/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_PhucLoi>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_PhucLoi>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string sttt,string tenphucloi, string ngayhieuluc, string ngayhethieuluc, string nguonchi,string mucchitoida, int trangthai)
        {
            try
            {
                DateTime ngay_hieu_luc = !string.IsNullOrEmpty(ngayhieuluc.Trim()) ? DateTime.ParseExact(ngayhieuluc, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;
                DateTime ngay_het_hieu_luc = !string.IsNullOrEmpty(ngayhethieuluc.Trim()) ? DateTime.ParseExact(ngayhethieuluc, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;
                
                DM_PhucLoi obj = new DM_PhucLoi();
                obj.SoTienTruocThue = sttt.Trim();
                obj.TenPhucLoi = tenphucloi;
                obj.NgayHieuLuc = ngay_hieu_luc;
                obj.NgayHetHieuLuc = ngay_het_hieu_luc;
                obj.NguonChi = nguonchi;
                obj.MucChiToiDa = mucchitoida.Trim();
                obj.TrangThai = trangthai;
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaCheDoPhucLoi");
                requestContent.Add(new StringContent(obj.SoTienTruocThue.ToString()), "SoTienTruocThue");
                requestContent.Add(new StringContent(obj.TenPhucLoi.ToString()), "TenPhucLoi");
                requestContent.Add(new StringContent(obj.NgayHieuLuc.ToString()), "NgayHieuLuc");
                requestContent.Add(new StringContent(obj.NgayHetHieuLuc.ToString()), "NgayHetHieuLuc");
                requestContent.Add(new StringContent(obj.NguonChi.ToString()), "NguonChi");
                requestContent.Add(new StringContent(obj.MucChiToiDa.ToString()), "MucChiToiDa");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_PhucLoi";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string ma, string sttt, string tenphucloi, string ngayhieuluc, string ngayhethieuluc, string nguonchi, string mucchitoida, int trangthai)
        {
            try
            {
                DateTime ngay_hieu_luc = !string.IsNullOrEmpty(ngayhieuluc.Trim()) ? DateTime.ParseExact(ngayhieuluc, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;
                DateTime ngay_het_hieu_luc = !string.IsNullOrEmpty(ngayhethieuluc.Trim()) ? DateTime.ParseExact(ngayhethieuluc, "yyyy-MM-dd", CultureInfo.CurrentCulture) : DateTime.Now;

                DM_PhucLoi obj = new DM_PhucLoi();
                obj.Id = id;
                obj.MaCheDoPhucLoi = ma;
                obj.SoTienTruocThue = sttt.Trim();
                obj.TenPhucLoi = tenphucloi;
                obj.NgayHieuLuc = ngay_hieu_luc;
                obj.NgayHetHieuLuc = ngay_het_hieu_luc;
                obj.NguonChi = nguonchi;
                obj.MucChiToiDa = mucchitoida.Trim();
                obj.TrangThai = trangthai;

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaCheDoPhucLoi), "MaCheDoPhucLoi");
                requestContent.Add(new StringContent(obj.SoTienTruocThue.ToString()), "SoTienTruocThue");
                requestContent.Add(new StringContent(obj.TenPhucLoi.ToString()), "TenPhucLoi");
                requestContent.Add(new StringContent(obj.NgayHieuLuc.ToString()), "NgayHieuLuc");
                requestContent.Add(new StringContent(obj.NgayHetHieuLuc.ToString()), "NgayHetHieuLuc");
                requestContent.Add(new StringContent(obj.NguonChi.ToString()), "NguonChi");
                requestContent.Add(new StringContent(obj.MucChiToiDa.ToString()), "MucChiToiDa");
                requestContent.Add(new StringContent(obj.TrangThai.ToString()), "TrangThai");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_PhucLoi/" + id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_PhucLoi/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}
