﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuanLyNhanSu.Common.Constants;
using QuanLyNhanSu.Model.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Controllers
{
    public class DM_TaiSanCapPhatController : Controller
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public DM_TaiSanCapPhatController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/DM_TaiSanCapPhat/GetAllRecords")).Result;

                var test = response.Content.ReadAsStringAsync().Result;
                var listdata = new List<QuanLyNhanSu.Model.Entities.DM_TaiSanCapPhat>();

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<QuanLyNhanSu.Model.Entities.DM_TaiSanCapPhat>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        public async Task<JsonResult> ThemMoi(string tentscp,int soluong,string mota)
        {
            try
            {
                DM_TaiSanCapPhat obj = new DM_TaiSanCapPhat();
                obj.TenTaiSanCapPhat = tentscp.Trim();
                obj.SoLuong = soluong;
                obj.MoTa = mota;
                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent("123"), "MaTaiSanCapPhat");
                requestContent.Add(new StringContent(obj.TenTaiSanCapPhat.ToString()), "TenTaiSanCapPhat");
                requestContent.Add(new StringContent(obj.SoLuong.ToString()), "SoLuong");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_TaiSanCapPhat";
                var response = await client.PostAsync(url, requestContent);

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới");
            }
        }

        public async Task<JsonResult> Sua(int id, string ma, string tentscp, int soluong, string mota)
        {
            try
            {
                DM_TaiSanCapPhat obj = new DM_TaiSanCapPhat();
                obj.Id = id;
                obj.MaTaiSanCapPhat = ma;
                obj.TenTaiSanCapPhat = tentscp.Trim();
                obj.SoLuong = soluong;
                obj.MoTa = mota;

                var sessions = _httpContextAccessor
               .HttpContext
               .Session
               .GetString(SystemConstants.AppSettings.Token);

                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var requestContent = new MultipartFormDataContent();
                requestContent.Add(new StringContent(obj.MaTaiSanCapPhat), "MaTaiSanCapPhat");
                requestContent.Add(new StringContent(obj.TenTaiSanCapPhat.ToString()), "TenTaiSanCapPhat");
                requestContent.Add(new StringContent(obj.SoLuong.ToString()), "SoLuong");
                requestContent.Add(new StringContent(obj.MoTa.ToString()), "MoTa");
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_TaiSanCapPhat/" + id;
                HttpResponseMessage response = Task.Run(async () => await client.PutAsync(url, requestContent)).Result;

                return Json("OK");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật: " + ex.Message;
                return Json("Lỗi cập nhật");
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString(SystemConstants.AppSettings.Token);
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = BASE_URL_API + "/api/DM_TaiSanCapPhat/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}

