﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Web.Models
{
    public class NhanVienViewModel
    {
        [Required(ErrorMessage = "Tên nhân viên không được bỏ trống.")]
        public string Ten { get; set; }
        [Required(ErrorMessage = "Chưa chọn giới tính")]
        public string GioiTinh { get; set; }
        [Required(ErrorMessage = "Ngày sinh nhân viên không được bỏ trống.")]
        public DateTime NgaySinh { get; set; }
        [Required(ErrorMessage = "Địa chỉ nhân viên không được bỏ trống.")]
        public string DiaChi { get; set; }
        [Required(ErrorMessage = "CCCD/CMND không được bỏ trống.")]
        public string CanCuoc_CMND { get; set; }
        [Required(ErrorMessage = "Ngày cấp CCCD/CMND không được bỏ trống.")]
        public DateTime NgayCap { get; set; }
        [Required(ErrorMessage = "Nơi cấp CCCD/CMND không được bỏ trống.")]
        public string NoiCap { get; set; }
        [Required(ErrorMessage = "Email không được bỏ trống.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Chưa chọn Chức danh")]
        public int ChucDanh { get; set; }
        [Required(ErrorMessage = "Chưa chọn Phòng ban")]
        public int PhongBan { get; set; }
        public int TrangThai { get; set; }

    }
}