﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyNhanSu.Web.Models
{
    public class NavigationViewModel
    {
        public string ReturnUrl { set; get; }
    }
}