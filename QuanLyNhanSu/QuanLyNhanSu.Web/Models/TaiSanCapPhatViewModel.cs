﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyNhanSu.Web.Models
{
    public class TaiSanCapPhatViewModel
    {
        public int Id { set; get; }
        public string MaTaiSanCapPhat { set; get; }
        public string TenTaiSanCapPhat { get; set; }
        public int SoLuong { set; get; }
        public int SoLuongConLai { set; get; }
        public string MoTa { get; set; }
    }
}